﻿Feature: Adjustments
##############################################################################################################
##
## This feature file covers the following:
## Adjustments tab is still disabled
## Adjustments tab is available
## <may add some more from here as needed>
##
## DON'T FORGET TO UPDATE THE SERVICEPROVIDER in KIM_SmokeTest>Data>Resources.resx.
## NOTE THAT SERVICE NEED TO BE IDENTIFIED IN THE TABLE BELOW (EXAMPLES: SECTION)
## ENSURE TO BUILD/REBUILD EVERYTIME THERE'S CHANGES IN THIS FILE
##
##############################################################################################################
####
## BELOW SCENARIO IS TO TEST ADJUSTMENTS TAB STILL DISABLED
## IN SERVICE SECTION BELOW (EXAMPLES), INPUT THE RELATED SERVICE WHERE YOU WISH TO TEST THE ADJUSTMENTS TAB
@adjustments
Scenario Outline: Adjustments_Disabled
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I click on adjustments tab
	And I verify and close the adjustment alert
	Then I verify the status for Teachers, Educators, Program, and Enrolments tabs are all Read Only and Annual Confirmation tab is Submitted
	And I close the browser successfully
	Examples:
	| service						|
	| Dolena Young Kindergarten	|
##
## BELOW SCENARIO IS TO TEST ADJUSTMENTS TAB AVAILABLE
## IN SERVICE SECTION BELOW (EXAMPLES), INPUT THE RELATED SERVICE WHERE YOU WISH TO TEST THE ADJUSTMENTS TAB
@adjustments
Scenario Outline: Adjustments_Available
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I verify the status for Teachers, Educators, Program, Enrolments, and Annual Confirmation tabs are all COMPLETE
	And I verify the adjustment tab status should be available
	And I click on adjustments tab
	Then I verify the available fields in Adjustments tab
	And I verify the correctness of values in Adjustments tab
	And I close the browser successfully
	Examples:
	| service						|
	| Wendouree Children's Services	|

﻿Feature: EskEnrolments
	Add an enrolment to a service or to multiple services
	Add multiple enrolments to a service or multiple services
	Edit an existing enrolment to a service
	Remove an existing enrolment from a service

	# The scenarios below are for ESK enrolments only so please change the dob if it needs to be
	# Change the name of enrolment, dob and address to create unique enrolments
	# Child commence date can be updated depending on the current date and month

@enrolment
Scenario Outline: AddESKEnrolment
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add enrolments for ESK
	| Given Name | Family Name | Sex  | Date of Birth  | Number | Street | StreetType | Suburb   | State | Postcode | ChildCommence  | EarlyStart KinderEligibility    | Early Childhood Teacher   | Hours | Minutes | weeks | Immunisation                                                         |
	| David      | Husk        | Male | " 24/04/2016 " | 9      | Lao    | ST         | laverton | VIC   | 3028     | " 02/03/2019 " | Child known to Child Protection | FstN10958 LstN10958 - YBS | 16    | 30      | 30    | The child has an up to date immunisation status certificate recorded | 
	And I close the browser successfully
	Examples:
	| service               |
	| Pax Hill Kindergarten | 

Scenario Outline: AddMultipleESKEnrolment
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add enrolments for ESK
    | Given Name | Family Name | Sex    | Date of Birth  | Number | Street | StreetType | Suburb    | State | Postcode | ChildCommence  | EarlyStart KinderEligibility    | Early Childhood Teacher | Hours | Minutes | weeks | Immunisation                                                         |
    | Will       | Watson      | Male   | " 02/06/2016 " | 9      | Goble  | ST         | laverton  | VIC   | 3028     | " 02/02/2019 " | Child known to Child Protection | James Wilson - ESK      | 16    | 30      | 30    | The child has an up to date immunisation status certificate recorded |
    | Roop       | Jain        | Female | " 11/11/2014 " | 4      | Bourke | ST         | Melbourne | VIC   | 3000     | " 02/03/2019 " | Child known to Child Protection | James Wilson - ESK      | 15    | 30      | 32    | The child has an up to date immunisation status certificate recorded |
	And I close the browser successfully
	Examples:
	| service                        |
	| Little River Kindergarten      |
@enrolment
Scenario Outline: EditESKEnrolment
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I open an existing eskenrolment with <SLK>
	And I edit <Street> of the eskenrolled student
	And I close the browser successfully
	Examples:
	| service                     |     SLK                  |  Street    |
	| Little River Kindergarten   | " US2AV230420161 "       |  Collins   | 

@enrolment
Scenario Outline: DeleteESKEnrolment
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I delete an existing enrolment with <name>
	And I close the browser successfully
	Examples:
	| service    | name             |
	| Happy Kids | Hardley Whittam  |
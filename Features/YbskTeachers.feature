﻿Feature: YbskTeachers
##############################################################################################################
##
## This feature file covers the following:
## Add a teacher to a service or to multiple services
## Add multiple teachers to a service or multiple services
## Edit an existing teacher to a service
## Remove an existing teacher from a service
## Show inactive only and make inactive teachers to active
## Show active teachers only
## <may add some more from here as needed>
##
## DON'T FORGET TO UPDATE THE SERVICEPROVIDER in KIM_SmokeTest>Data>Resources.resx.
## NOTE THAT SERVICE NEED TO IDENTIFIED IN THE TABLE BELOW (EXAMPLES: SECTION)
## DETAILS FOR NEW TEACHER, TEACHER TO BE MODIFIED/DELETED NEED TO BE IDENTIFIED IN BELOW TABLE
## ENSURE TO BUILD/REBUILD EVERYTIME THERE'S CHANGES IN THIS FILE
##############################################################################################################
##
## BELOW SCENARIO IS TO TEST ADD TEACHER
## IN TABLE BELOW, INPUT THE DETAILS FOR TEACHER THAT WILL BE ADDED
## IN SERVICE SECTION BELOW (EXAMPLES), INPUT THE RELATED SERVICE WHERE YOU WISH TO HAVE TEACHER BE ADDED
@teacher
Scenario Outline: AddTeacher
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add teachers
	| Given Name | Family Name | Gender | Date of Birth  | CommencementDate | RegistrationStatus       | VITNumber | University      | Course                          | Year | Groups | TeacherShare | IndustrialAgreement                             | Level | hours |
	| Hill       | William     | Male   | " 11/11/1982 " | " 13/10/2018 "   | Fully registered teacher | 40777     | Bond University | Bachelor of Children's Services | 2008 | 5      | No           | Early Education Employees Agreement 2016 (EEEA) | 1.1   | 5     |
	And I close the browser successfully
	Examples:
	| service     |
	| 12 New kids |
##
## BELOW SCENARIO IS TO TEST ADD MULTIPLE TEACHER
## IN TABLE BELOW, INPUT THE DETAILS FOR THE TEACHERS THAT WILL BE ADDED
## IN SERVICE SECTION BELOW (EXAMPLES), INPUT THE RELATED SERVICE WHERE YOU WISH TO HAVE TEACHER BE ADDED
@teacher
Scenario Outline: AddMultipleTeacher
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add teachers
	| Given Name | Family Name | Gender | Date of Birth   | CommencementDate | RegistrationStatus       | VITNumber | University      | Course                          | Year | Groups | TeacherShare | IndustrialAgreement                             | Level | hours |
	| Mary       | Halls       | Female | " 04/05/1983 "  | " 03/04/2018 "   | Fully registered teacher | 4052081   | Bond University | Bachelor of Children's Services | 2008 | 5      | No           | Early Education Employees Agreement 2016 (EEEA) | 1.1   | 1     |
	| Harry      |Porter       | Male   | " 09/05/1984 "  | " 05/02/2017 "    | Fully registered teacher | 4050015    | Bond University | Bachelor of Children's Services | 2009 | 5      | No           | Early Education Employees Agreement 2016 (EEEA) | 1.1   | 2     |    
	| Andrew     | Joe         | Male   | " 06/05/1982 "  | " 07/05/2016 "    | Fully registered teacher | 4055015    | Bond University | Bachelor of Children's Services | 2009 | 5      | No           | Early Education Employees Agreement 2016 (EEEA) | 1.1   | 2     | 
	And I close the browser successfully
	Examples:
	| service            |
	| Strathmore kids    |
##
## BELOW SCENARIO IS TO TEST EDIT TEACHER
## IN TABLE BELOW, INPUT THE DETAILS FOR THE TEACHERS THAT WILL BE EDITED
## IN SERVICE SECTION BELOW (EXAMPLES), INPUT THE RELATED SERVICE WHERE YOU WISH TO HAVE TEACHER BE EDITED
@teacher
Scenario Outline: EditTeacher
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I open an existing teacher with <name>
	And I edit <commencedate> of teacher
	And I edit number of <groups> 
	And I close the browser successfully
	Examples:
	| service          | name             | commencedate | groups  |
	| Strathmore kids  | Andrew Joe       | "24/03/2019" |   61    |
##
## BELOW SCENARIO IS TO TEST DELETE TEACHER
## IN TABLE BELOW, INPUT THE DETAILS FOR THE TEACHERS THAT WILL BE DELETED
## IN SERVICE SECTION BELOW (EXAMPLES), INPUT THE RELATED SERVICE WHERE YOU WISH TO HAVE TEACHER BE DELETED FROM
@teacher
Scenario Outline: DeleteTeacher
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I delete an existing teacher with <name>
	And I enter final date and <reason> for leaving and confirm removal
	And I verify that teacher with <name> is deleted successfully 
	And I close the browser successfully
	Examples:
	| service    | name              | reason   |
	| Happy Kids | Oliver William    | Resigned |
##
## BELOW SCENARIO IS TO TEST APPLICATION OF LEAVE FOR TEACHERS
## IN TABLE BELOW, INPUT THE DETAILS FOR THE TEACHERS THAT WILL BE ADDED
## IN SERVICE SECTION BELOW (EXAMPLES), INPUT THE RELATED SERVICE WHERE YOU WISH TO HAVE TEACHER BE ADDED	
@teacher
Scenario Outline: ApplicationTeacher
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I open an existing teacher to apply for parentalleave  with <name>
	And  I fill teachers application for parental leave
	|  Date From      |   Date To        | HourlyRate | Weekly Hours1 | Weekly Hours2 | Name        | Position          | 
	| " 01/02/2019 "  | " 04/04/2019 "   |   50       | 40            | 40            | Scott Martin| Senior Manager    |  
	And I close the browser successfully
	Examples:
	| service         | name       |
	| Strathmore kids | Mary Halls |
##
## BELOW SCENARIO IS TO TEST SHOW INACTIVE TEACHER
## IN TABLE BELOW, INPUT THE DETAILS FOR THE TEACHERS THAT WILL BE ADDED
## IN SERVICE SECTION BELOW (EXAMPLES), INPUT THE RELATED SERVICE WHERE YOU WISH TO HAVE TEACHER BE ADDED
@teacher
Scenario Outline: ShowInactiveMakeitActiveTeacher
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add click show inactive and make inactive teacher	to active
	And I close the browser successfully
	Examples:
	| service    |
	| The Grange Kindergarten |

@teacher
Scenario Outline: CheckthenumberofTeacher
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I check the number of teacher in Teachers tab	
	And I close the browser successfully
	Examples: 
	| service          |
	| NCL SP2706010318 |

###Update the ServiceTravel depending on the service provider used in your test
###Service name should be updated based on test performed
###Name refers to teacher name so should be updated accordingly
Scenario Outline: Teacher_ApplicationTravelAllowance
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I open an existing teacher to apply for Travel Allowance with <name>
	And  I fill teachers application for travel leave
	| ServiceTravel | FirstDay_Travel | Weeks_Year1 | Weeks_Year2 | Kms |Name         | Position       |
	| Eym Test      | "16/07/2019"    | 5           | 4           | 10  |Scott Martin | Senior Manager |
	And I close the browser successfully
	Examples:
	| service                         | name              |
	| Strathmore Heights Kindergarten | FstN4734 LstN4734 |
﻿Feature: IntegratedJobs
	Automate the jobs running from Kim sharepoint to crm and vice versa

	#To import any Service information submitted in KIM SharePoint to Kim crm
    @functional
Scenario: Run KIM sharepoint to crm service
	Given I go to manage job url
	When I run the services batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

	# To update the CRM Status of a record in KIM SharePoint
	@functional
Scenario: Run KIM crm message to KIM sharepoint
	Given I go to manage job url
	When I run the messages batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully
	
	# To import ESK Teacher, ESK Program and ESK Child records to Kim CRM
	@functional
Scenario: Run ESK wrapper job
	Given I go to manage job url
	When I run the esk wrapper job to import programs, child etc to kim crm
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

	# To import YBSK data to Kim Crm
	@functional
Scenario: Run YBSK wrapper job
	Given I go to manage job url
	When I run the ybsk wrapper job to import programs, child etc to kim crm
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

	# To initiate the enrolment confirmation process for an EC Service
    @functional
Scenario: Run Enrolment Confirmation job
	Given I go to manage job url
	When I run the enrolment confirmation job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

		# To initiate the parental leave job
		## The job imports the parental leave to kim crm
    @functional
Scenario: Run parental leave job
	Given I go to manage job url
	When I run the parental leave job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

		# To initiate the travel allowance leave job
		## The job imports the travel leave to kim crm
    @functional
Scenario: Run travel allowance leave job
	Given I go to manage job url
	When I run the travel leave job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully
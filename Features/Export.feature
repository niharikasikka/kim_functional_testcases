﻿Feature: ExporttoExcel
############################################################################################################
## use this feature to test the export to excel
## this includes the export to excel for ESK Teachers and Enrolments
## this also includes the export to excel for YBSK Teachers, Other Educators, Programs and Enrolments
##
## Notes:
## Don't forget to update the DownloadDir in KIM_SmokeTest>Data>Resources.resx.  
## Update DownloadDir with value based on browser's download directory set in the Settings
## for example in Chrome, go to Settings>Download
## value here should be the value for DownloadDir
## Don't forget to set Settings>Download>"Ask where to save each file before downloading" to disabled/off
## Exported file goes directly DownloadDir and there's validation involved whether file was created there
###########################################################################################################
##
## BELOW SCENARIO IS TO TEST EXPORT TO EXCEL "ESK" TEACHER AND ENROLMENT DATA
## IN SERVICE SECTION, INPUT THE RELATED SERVICE YOU WISH TO HAVE TEACHER AND ENROLMENT DATA EXPORTED TO EXCEL
## DON'T FORGET TO UPDATE THE SERVICEPROVIDER in KIM_SmokeTest>Data>Resources.resx. 
@export
Scenario Outline: ESKExport_Teachers_and_Enrolment
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I export teachers and enrolment data
Examples:
	| service                        |
	| Laverton Child Kindergarten    |
##
## BELOW SCENARIO IS TO TEST EXPORT TO EXCEL "YBSK" TEACHER, OTHER EDUCATOR, PROGRAM AND ENROLMENT DATA
## IN SERVICE SECTION, INPUT THE RELATED SERVICE YOU WISH TO HAVE TEACHER, OTHER EDUCATOR, PROGRAM AND 
## ENROLMENT DATA EXPORTED TO EXCEL
## DON'T FORGET TO UPDATE THE SERVICEPROVIDER in KIM_SmokeTest>Data>Resources.resx.
@export 
Scenario Outline: YBSKExport_Teachers_otherEducators_Programs_and_Enrolments
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint	
	And I export teachers data
	And I export other educators data
	And I export programs data
	And I export enrolments data
Examples:
	| service                          |
	| Lindenow & District Kindergarten | 
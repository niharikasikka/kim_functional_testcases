﻿Feature: Tasks
         Automating different tasks in kim crm 
		 Roles involved: System admin, RO Kim editor, RO Kim approver, CO Kim editor, CO Kim approver

###################################################################################################################################################
###
###Following tasks are automated as per roles
###
###ACS-1 task EC Service – application to add an additional EYM service as CO_KimEditor
###ACS-2 task EC Service – EYM status has changed as CO_KimApprover
###CSK-3 (EC Service – Approval of request to remove a service from K1) as kim region approver
###UKC-2 task (UKC-2	Child – possible duplicate record) as RO_KimEditor
###ECF-13	Annual confirmation – No Qualified Teacher as RO_KimEditor
##ECF-15	Annual confirmation – less than 600 hours as RO_KimEditor
##ECF-36	Annual confirmation – no written confirmation to parents as RO_KimEditor
###CSK-2 EC Service–Request to remove a service rejected–cease date incorrect
###CSK-4 (EC Service – A request to cease per capita funding has been approved.  Confirm if EYM funding  is to cease or continue) as CO kim editor
###RCS-2 EC Service – Approve Request to cease EYM Funding as CO kim approver
###ECF-22	Annual confirmation – Annual confirmation after Confirmation Period task
###
###
######################################################################################################################################################

## As Kim editor - central office, complete ACS-1 task
## ACS task: EC Service – application to add an additional EYM service 
## Please note should only be run with Kim editor user details
@Eymtasks
Scenario Outline: KimCrm_ACS1Task_CO_KimEditor
	Given I login to Kim crm application as CO kim editor
	When As a Kim editor I search for added service <service>
	And Kim editor completes the task and change the status
	| Eym Start Date | Eym Funding Start Date | Eym Funding End Date | Notes          | Action                          |
	| 20/06/2019     | 20/06/2019             | 20/06/2020           | Please approve | Request Central Office Approval |
	Then Kim editor marks the <task> complete
	And I logout of Kim crm application
	And I close the browser successfully
	Examples:
	| service  | task                                                      |
	| Eym Test | EC Service – application to add an additional EYM service |

## As Kim approver - central office, approve ACS-2 task
## ACS task: EC Service – EYM status has changed 
@Eymtasks
Scenario Outline: KimCrm_ACS2Task_CO_KimApprover
	Given I login to Kim crm application as CO kim approver
	When As a Kim approver I search for added service <service>
	And Kim approver checks for data and changes the status to Approve
	Then Kim approver marks the <task> complete
	And I logout of Kim crm application
	And I close the browser successfully
	Examples:
	| service  | task                   |
	|Mock1 Test | EYM status has changed |

###RO Kim approver approves the service cease request and mark the task complete
Scenario Outline: KimCrm_CeaseService_RO_KimApprover_WithTask
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>
	And approver approves the <service> with <status>
	Then Kim approver marks the <task> complete
	And I logout of Kim crm application
	And I close the browser successfully
	Examples: 
	| service     | status  | task                                            |
	| 23 New kids | Approve | Approval of request to remove a service from K1 |

##As RO kim editor, complete the pending task and mark as complete
Scenario Outline: KimCrm_DuplicateEnrol_RO_KimEditor
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	When Kim editor navigate to duplicate enrolment and go to activities
	| Children - SLK | note                                          |
	| HITAM041020141 | Duplicate record informed to service provider |
	Then Kim editor marks the <task> complete for enrolment 
	Then I logout of Kim crm application 
	And I close the browser successfully
	Examples: 
	| service     | task                              |
	| 12 New kids | Child – possible duplicate record |

##As regional Kim editor enter a note for the task: Annual confirmation – No Qualified Teacher 
##Mark the task as complete
Scenario Outline: KimCrm_ECF13_RO_KimEditor
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	And Kim RO editor completes the task with <noteTeacher>
	Then Kim editor marks the <NoQualifiedTeacher> complete
	And I logout of Kim crm application
	And I close the browser successfully
	Examples:
	| service  | NoQualifiedTeacher                         | noteTeacher                     |
	| Eym Test | Annual confirmation – No Qualified Teacher | Teacher qualifications modified |

##As regional Kim editor enter a note for the task: Annual confirmation – less than 600 hours
##Mark the task as complete
Scenario Outline: KimCrm_ECF15_RO_KimEditor
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	When Kim RO editor completes the task with <noteNumberHours>
	Then Kim editor marks the <LessHours> complete
	And I logout of Kim crm application
	And I close the browser successfully
	Examples:
	| service  | LessHours                                 | noteNumberHours                      |
	| Eym Test | Annual confirmation – less than 600 hours | Service confirms more than 600 hours | 

##As regional Kim editor enter a note for the task: Annual confirmation – no written confirmation to parents
##Mark the task as complete
Scenario Outline: KimCrm_ECF36_RO_KimEditor
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	When Kim RO editor completes the task with <noteWrittenComm>
	Then Kim editor marks the <NoWrittenComm> complete
	And I logout of Kim crm application
	And I close the browser successfully
	Examples:
	| service  | NoWrittenComm                                            | noteWrittenComm                           |
	| Eym Test | Annual confirmation – no written confirmation to parents | Written communication to parents provided | 

###As CO Kim editor cease the eym funding to the ceased service
###Change the EYM Managed Service field to 'No', enter a EYM End Date
###change the EYM Funding End Date if required, update the Action to 'Approve'
##This script updates the eym end date and eym funding end date as current date
Scenario Outline: KimCrm_CSK4_CO_KimEditor
	Given I login to Kim crm application as CO kim editor
	When As a Kim editor I search for added service <service>
	And Kim editor completes the task and change the status to CO approver request
	Then Kim editor marks the <task> complete
	And I logout of Kim crm application
	And I close the browser successfully
	Examples:
	| service     | task                                                    |
	| 23 New kids | A request to cease per capita funding has been approved |

###As CO Kim approver, approve the request to cease eym funding
###Complete the task RCS-2 EC Service – Approve Request to cease EYM Funding
Scenario Outline: KimCrm_RCS2Task_CO_KimApprover
	Given I login to Kim crm application as CO kim approver
	When As a Kim approver I search for added service <service>
	##And Kim approver checks for data and changes the status to Approve
	Then Kim approver marks the <task> complete
	And I logout of Kim crm application
	And I close the browser successfully
	Examples:
	| service     | task                                 |
	| 23 New kids | Approve Request to cease EYM funding |

###As RO Kim approver, approve the ECF-22	Annual confirmation – Annual confirmation after Confirmation Period task
Scenario Outline: KimCrmPreCommitments_ApprovePrecommitment_WithTask_ECF22
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>
    Then I verify the confirm <status> of funding information
	And I approve the following precommitments and funding category with task <task> and <note>
	| Funding Category | Action    |
	| Per Capita       | 864710007 |
	##| Kindergarten Fee Subsidy | 864710007	|
	##| ESK EXT CP               | 864710007	|
	## below are the corresponding value for each action
	## 864710007 - Approve
	## 864710005 - More Information
	## 864710002 - Reject to Region
	## 864710001 - Request Central Office Approval
	And I logout of Kim crm application
	And I close the browser successfully
Examples: 
	| service                         | task                                          | status      | note                    |
	| Strathmore Heights Kindergarten | Annual confirmation after Confirmation Period | In Progress | Approved pre commitment | 

###RO Kim editor is informed of cease service request rejected and mark the task complete
###Task CSK1 EC Service – Request to remove a service rejected – update in progress
Scenario Outline: KimCrm_CeaseService_RO_KimEditor_CSK1
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	When Kim RO editor completes the task with <note>
	Then Kim editor marks the <task> complete
	Then I logout of Kim crm application
	And I close the browser successfully
	Examples: 
	| service  | note                      | task                                                      |
	| NCL SP04 | Cease service is rejected | Request to remove a service rejected – update in progress |

###RO Kim editor is informed of cease service request rejected and mark the task complete
###Review CSK-2 (EC Service – Request to remove a service rejected – cease date incorrect) as kim region editor
Scenario Outline: KimCrm_CeaseService_RO_KimEditor_CSK2
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	When Kim RO editor completes the task with <note>
	Then Kim editor marks the <task> complete
	Then I logout of Kim crm application
	And I close the browser successfully
	Examples: 
	| service  | note                      | task                                                        |
	| NCL SP04 | Cease service is rejected | Request to remove a service rejected – cease date incorrect |


######################################################################
##Please enter unique service approval id
##The below test works for only south western victoria region services
##In order to make it work for different region change region below 
## &
##Update the ROeditor userid and password in resources file
##Task NSK-6	EC Service – Verify application
######################################################################
@smoke1
Scenario Outline: 03 KimCrm_NewService_RO_KimEditor
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	And editor completes the pending tasks
	| Service Region         | Service Category | Service Lga        | Service ApprovalId | Action                   |
	| South Western Vistoria | Standard         | Moonee Valley City | <autogenerated>    | Request Manager Approval |  
	Then Kim editor marks the <task> complete
	Then I logout of Kim crm application 
	And I close the browser successfully
	Examples: 
	| service   | task               |
	| Mock Test | Verify application |

##############################################################
##Following test scenario runs for kim approver
##Login as kim regional approver and approves the new service
###Task NSK-7	EC Service – Approve new or transferred service
###############################################################
@smoke1
Scenario Outline: 04 KimCrm_NewService_RO_KimApprover
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>
	And approver approves the <service> with <status>
	Then Kim approver marks the <task> complete
	Then the service is ready for funding
	And I logout of Kim crm application
	And I close the browser successfully
	Examples: 
	| service   | status  | task                               |
	| Mock Test | Approve | Approve new or transferred service |
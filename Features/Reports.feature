﻿Feature: Reports
	Download report	
############################################################################################################
## use this feature to test the downloading of reports
## this includes the following reports -
## 
## MAIN REPORTS:
## AnnualConfirmationReport
## ESKSurveyReport
## GeoCodeSummaryReport
## NonEnglishSpeakingReport
## RotationalGroupsReport
## SessionalGroupsReport
## SPGOverviewReport
## 
## SERVICE PROVIDER REPORT:
## ServiceProviderFundingCommitmentReport
##
## SERVICE REPORTS:
## FormsByServiceLocationReport
## ServiceSummaryReport
## 
## Notes:
## Don't forget to update the DownloadDir in KIM_SmokeTest>Data>Resources.resx.  
## Update DownloadDir with value based on browser's download directory set in the Settings
## for example in Chrome, go to Settings>Download
## value here should be the value for DownloadDir
## Don't forget to set Settings>Download>"Ask where to save each file before downloading" to disabled/off
## Some downloaded report files goes directly DownloadDir and there's validation involved whether file was created there
###########################################################################################################
##
## BELOW SCENARIO IS TO TEST ANNUAL CONFIRMATION REPORT
@reports
Scenario: AnnualConfirmationReport
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on reports
	Then I download Annual Confirmation report
	And I close the browser successfully
##
## BELOW SCENARIO IS TO TEST ESK SURVEY REPORT
@reports
Scenario: ESKSurveyReport
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on reports
	Then I download ESK Survey report
	And I close the browser successfully
##
## BELOW SCENARIO IS TO TEST GEOCODE SUMMARY REPORT
@reports
Scenario: GeoCodeSummaryReport
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on reports
	Then I download Geo Code Summary for Enrolments report
	And I close the browser successfully
##
## BELOW SCENARIO IS TO TEST NON ENGLISH SPEAKING REPORT
@reports
Scenario: NonEnglishSpeakingReport
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on reports
	Then I download Non English Speaking Background report
	And I close the browser successfully
##
## BELOW SCENARIO IS TO TEST ROTATIONAL GROUPS REPORT
@reports
Scenario: RotationalGroupsReport
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on reports
	Then I download Rotational groups extract for ratio funding report
	And I close the browser successfully
##
## BELOW SCENARIO IS TO TEST SESSIONAL GROUPS REPORT
@reports
Scenario: SessionalGroupsReport
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on reports
	Then I download Sessional groups report for ratio funding
	And I close the browser successfully
##
## BELOW SCENARIO IS TO TEST SPG OVERVIEW REPORT
@reports
Scenario: SPGOverviewReport
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on reports
	Then I download SPG Overview Report
	And I close the browser successfully
##
## BELOW SCENARIO IS TO TEST SERVICE PROVIDER FUNDING COMMITMENT REPORT
## DON'T FORGET TO UPDATE THE SERVICEPROVIDER in KIM_SmokeTest>Data>Resources.resx. 
## WITH THE SERVICE PROVIDER THAT YOU WISH TO SEE THE REPORT
@reports
Scenario: ServiceProviderFundingCommitmentReport
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	And I click on Service Provider report
	Then I download Service Provider Funding Commitment Report
	And I close the browser successfully
##
## BELOW SCENARIO IS TO TEST FORMS BY SERVICE LOCATION REPORT
## DON'T FORGET TO UPDATE THE SERVICEPROVIDER in KIM_SmokeTest>Data>Resources.resx. 
## WITH THE SERVICE PROVIDER THAT YOU WISH TO SEE THE REPORT
@reports
Scenario: FormsByServiceLocationReport
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	And I click on Service report
	Then I download Forms By Service Location Report
	And I close the browser successfully
##
## BELOW SCENARIO IS TO TEST SERVICE SUMMARY REPORT
## DON'T FORGET TO UPDATE THE SERVICEPROVIDER in KIM_SmokeTest>Data>Resources.resx. 
## WITH THE SERVICE PROVIDER THAT YOU WISH TO SEE THE REPORT
@reports
Scenario: ServiceSummaryReport
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	And I click on Service report
	Then I download Service Summary Report
	And I close the browser successfully
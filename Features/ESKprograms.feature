﻿Feature: EskPrograms
	Add a Program to a service or to multiple services
	
	# Change the service name as per requirement

@enrolment
Scenario Outline: AddESKProgram
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add Programs for ESK
	| Hours | Mins | Weeks | Fees | Fee Period |
	| 16    | 30   | 40    | 420  | Per term   |
	And I close the browser successfully
	Examples:
	| service    |
	| Happy kids |

﻿Feature: YbskEducators
	Add an educator to a service or to multiple services
	Add multiple educators to a service or multiple services
	Edit an existing educator to a service
	Remove an existing educator from a service by name
##############################################################################################################
##
## This feature file covers the following:
## Add an educator to a service or to multiple services
## Add multiple educators to a service or multiple services
## Edit an existing educator to a service
## Remove an existing educator from a service
## Show inactive only and make inactive educators to active
## Show active educators only
## <may add some more from here as needed>
##
## DON'T FORGET TO UPDATE THE SERVICEPROVIDER in KIM_SmokeTest>Data>Resources.resx.
## NOTE THAT SERVICE NEED TO IDENTIFIED IN THE TABLE BELOW (EXAMPLES: SECTION)
## DETAILS FOR NEW EDUCATOR, EDUCATOR TO BE MODIFIED/DELETED NEED TO BE IDENTIFIED IN BELOW TABLE
## ENSURE TO BUILD/REBUILD EVERYTIME THERE'S CHANGES IN THIS FILE
##############################################################################################################
##
## BELOW SCENARIO IS TO TEST ADD EDUCATOR
## IN TABLE BELOW, INPUT THE DETAILS FOR EDUCATOR THAT WILL BE ADDED
## IN SERVICE SECTION BELOW (EXAMPLES), INPUT THE RELATED SERVICE WHERE YOU WISH TO HAVE EDUCATOR BE ADDED
@educator
Scenario Outline: AddEducator
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add educators
	| Given Name | Family Name | Gender | Date of Birth  | HighestTeachingQualification | IndustrialAgreement                          | Level | ProfDevHours | HoursofEmployment | HoursWorkedinAnyprogram |
	| Tobi        | Half      | Male   | " 01/09/1988 " | Kindercraft Assistant        | A local council agreement with EEEA appended | 1.1   | 80           | 40                | 60                      |
	And I close the browser successfully
	Examples:
	| service           |
	| Moe P.L.A.C.E.    |
##	
##
## BELOW SCENARIO IS TO TEST ADD MULTIPLE EDUCATOR
## IN TABLE BELOW, INPUT THE DETAILS FOR THE EDUCATORS THAT WILL BE ADDED
## IN SERVICE SECTION BELOW (EXAMPLES), INPUT THE RELATED SERVICE WHERE YOU WISH TO HAVE EDUCATOR BE ADDED
@educator
Scenario Outline: AddMultipleEducator
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add educators
	| Given Name  | Family Name | Gender | Date of Birth  | HighestTeachingQualification | IndustrialAgreement                          | Level | ProfDevHours | HoursofEmployment | HoursWorkedinAnyprogram |
	| Timm1        | Hafner1      | Male   | " 25/06/1984 " | Kindercraft Assistant        | A local council agreement with EEEA appended | 1.1   | 80           | 40                | 50                      |
	| Tomm1       | Hafner1      | Male   | " 25/06/1984 " | Kindercraft Assistant        | A local council agreement with EEEA appended | 1.1   | 80           | 40                | 20                      |
	And I close the browser successfully
	Examples:
	| service        |
	| Happy Kids     |
##
## BELOW SCENARIO IS TO TEST EDIT EDUCATOR
## IN TABLE BELOW, INPUT THE DETAILS FOR THE EDUCATORS THAT WILL BE EDITED
## IN SERVICE SECTION BELOW (EXAMPLES), INPUT THE RELATED SERVICE WHERE YOU WISH TO HAVE EDUCATOR BE EDITED
@educator
Scenario Outline: EditEducator
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I open an existing educator with <name>
	And I edit <dob> and number of employment <hours> of an educator  
	And I close the browser successfully
	Examples:
	| service                  | name          | dob           | hours    |
	| Moonee Kids Kindergarten | Tobby Hafneri |" 29/04/1991 " |   70     |
	| Happy Kids               | Tob Hafner    |" 29/01/1990 " |   90     |
##
## BELOW SCENARIO IS TO TEST DELETE EDUCATOR
## IN TABLE BELOW, INPUT THE DETAILS FOR THE EDUCATORS THAT WILL BE DELETED
## IN SERVICE SECTION BELOW (EXAMPLES), INPUT THE RELATED SERVICE WHERE YOU WISH TO HAVE EDUCATOR BE DELETED FROM
@educator
Scenario Outline: DeleteEducator
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I delete an existing educator <name> with <reason>
	And I verify that educator <name> is deleted successfully 
	And I close the browser successfully
	Examples:
	| service                  | name      | reason   |
	| Moonee Kids Kindergarten | Tobi Half | Resigned |
##
## BELOW SCENARIO IS TO TEST SHOW INACTIVE EDUCATOR
## IN TABLE BELOW, INPUT THE DETAILS FOR THE EDUCATORS THAT WILL BE ADDED
## IN SERVICE SECTION BELOW (EXAMPLES), INPUT THE RELATED SERVICE WHERE YOU WISH TO HAVE EDUCATOR BE ADDED
@educator
Scenario Outline: ShowInactiveMakeitActiveEducator
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add click show inactive and make inactive educator to active
	And I close the browser successfully
	Examples:
	| service    |
	| The Grange Kindergarten |

@educator
Scenario Outline: CheckthenumberofTeacher
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint	
	And I check the number of educator in Other Educators tab
	And I close the browser successfully
	Examples: 
	| service          |
	| NCL SP2706010318 |
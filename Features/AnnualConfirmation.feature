﻿Feature: AnnualConfirmation
	TEPE = Teachers, Educators, Program, and/or Enrolments
	Confirm services with TEPE flagged as COMPLETE and not yet been processed but Annual Confirmed not yet complete
	Confirm services with TEPE flagged as COMPLETE and has been processed but Annual Confirmed not yet complete
	Confirm services Annual Confirm with TEPE NOT flagged as COMPLETE and Annual Confirmed not yet complete
	Confirm services with TEPE flagged as COMPLETE and has been processed and Annual Confirmed complete

	# Change the service name to test in all the scenarios before running the scripts
	# Build/Rebuild the solution
	# If need to test for different number of children enrolled change in the data below
	# Each scenario can be run for 1 or more services and 1 or more data tables
	# For multiple service test : |service|
	#                             |Pax Hill Kindergarten|
	#							  |Maryvale Crescent Kindergarten|

@annualconfirmation
Scenario Outline: AnnualConfirm_Integrated
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I check the status for Teachers, Educators, Program, and Enrolments tabs all COMPLETE
	Then I verify the status for Annual Confirmation should be COMPLETE
	And I click on annual confirmation tab
	Then I verify the available fields in annual confirmation tab
	And I verify the values in annual confirmation tab
	And I click confirm and submit the data
	| TLDS completed | 0 TLDS completed forwarded to schools | 3 year old children enrolled | no of hours for 3 year old | Total Other Early | Masters or above | Graduate diploma | Dual bachelors | Bachelor | Pathway | Diploma | CertName | CertPosition | CertDate   |
	| 0              | 0                                     | 0                            | 0                          | 0                 | 0                | 0                | 0              | 0        | 0       | 0       | TestName | TestPosition | 01/08/2013 |
	Then I verify the status for Teachers, Educators, Program, and Enrolments tabs all Read Only
	And I close the browser successfully	
	Examples:
	| service                           |
	| Montgomery Park Children's Centre |


@annualconfirmation
Scenario Outline: AnnualConfirm_Disabled
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I click on annual confirmation tab
	And I verify and close the alert
	Then I verify the status for Teachers, Educators, Program, and Enrolments tabs all Read Only
	And I close the browser successfully
	Examples:
	| service	|
	| NCL SP02	| 

@annualconfirmation
Scenario Outline: AnnualConfirm_Sessional_4yo
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I check the status for Teachers, Educators, Program, and Enrolments tabs all COMPLETE
	Then I verify the status for Annual Confirmation should be COMPLETE
	And I click on annual confirmation tab
	Then I verify the available fields in annual confirmation tab
	And I verify the values in annual confirmation tab
	And I click confirm and submit the data for sessional program
	| TLDS completed | 0 TLDS completed forwarded to schools | 3 year old children enrolled | no of hours for 3 year old | Total Other Early | Masters or above | Graduate diploma | Dual bachelors | Bachelor | Pathway | Diploma | CertName | CertPosition | CertDate   |
	| 0              | 0                                     | 0                            | 0                          | 0                 | 0                | 0                | 0              | 0        | 0       | 0       | TestName | TestPosition | 01/08/2013 |
	Then I verify the status for Teachers, Educators, Program, and Enrolments tabs all Read Only
	And I close the browser successfully	
	Examples:
	| service                           |
	| Montgomery Park Children's Centre |

@annualconfirmation
Scenario Outline: AnnualConfirm_Sessional_4yo3yoSeparate
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I check the status for Teachers, Educators, Program, and Enrolments tabs all COMPLETE
	Then I verify the status for Annual Confirmation should be COMPLETE
	And I click on annual confirmation tab
	Then I verify the available fields in annual confirmation tab
	And I verify the values in annual confirmation tab
	And I click confirm and submit the data for sessional program - Separate
	| TLDS completed | 0 TLDS completed forwarded to schools | 3 year old children enrolled | no of hours for 3 year old | Total Other Early | Masters or above | Graduate diploma | Dual bachelors | Bachelor | Pathway | Diploma | CertName | CertPosition | CertDate   |
	| 0              | 0                                     | 0                            | 0                          | 0                 | 0                | 0                | 0              | 0        | 0       | 0       | TestName | TestPosition | 01/08/2013 |
	Then I verify the status for Teachers, Educators, Program, and Enrolments tabs all Read Only	
	And I close the browser successfully	
	Examples:
	| service               |
	| Lindenow & District Kindergarten | 

@annualconfirmation
Scenario Outline: AnnualConfirm_Sessional_4yo3yoCombined
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I check the status for Teachers, Educators, Program, and Enrolments tabs all COMPLETE
	Then I verify the status for Annual Confirmation should be COMPLETE
	And I click on annual confirmation tab
	Then I verify the available fields in annual confirmation tab
	And I verify the values in annual confirmation tab
	And I click confirm and submit the data for sessional program Combined
	| TLDS completed | 0 TLDS completed forwarded to schools | 3 year old children enrolled | no of hours for 3 year old | Total Other Early | Masters or above | Graduate diploma | Dual bachelors | Bachelor | Pathway | Diploma | CertName | CertPosition | CertDate   |
	| 0              | 0                                     | 0                            | 0                          | 0                 | 0                | 0                | 0              | 0        | 0       | 0       | TestName | TestPosition | 01/08/2013 |
	Then I verify the status for Teachers, Educators, Program, and Enrolments tabs all Read Only
	And I close the browser successfully	
	Examples:
	| service               |
	| Thorpdale Kindergarten | 

@annualconfirmation
Scenario Outline: AnnualConfirm_Sessional_Rotational_4yo
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I check the status for Teachers, Educators, Program, and Enrolments tabs all COMPLETE
	Then I verify the status for Annual Confirmation should be COMPLETE
	And I click on annual confirmation tab
	Then I verify the available fields in annual confirmation tab
	And I verify the values in annual confirmation tab
	And I click confirm and submit the data for sessional rotational program
	| TLDS completed | 0 TLDS completed forwarded to schools | 3 year old children enrolled | no of hours for 3 year old | Total Other Early | Masters or above | Graduate diploma | Dual bachelors | Bachelor | Pathway | Diploma | CertName | CertPosition | CertDate   |
	| 0              | 0                                     | 0                            | 0                          | 0                 | 0                | 0                | 0              | 0        | 0       | 0       | TestName | TestPosition | 01/08/2013 |
	Then I verify the status for Teachers, Educators, Program, and Enrolments tabs all Read Only
	And I close the browser successfully	
	Examples:
	| service                 |
	| Wendouree Children's Services |  

@annualconfirmation
Scenario Outline: AnnualConfirm_Sessional_Rotational_4yo3yoSeparate
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I check the status for Teachers, Educators, Program, and Enrolments tabs all COMPLETE
	Then I verify the status for Annual Confirmation should be COMPLETE
	And I click on annual confirmation tab
	Then I verify the available fields in annual confirmation tab
	And I verify the values in annual confirmation tab
	And I click confirm and submit the data for sessional rotational program - Separate
	| TLDS completed | 0 TLDS completed forwarded to schools | 3 year old children enrolled | no of hours for 3 year old | Total Other Early | Masters or above | Graduate diploma | Dual bachelors | Bachelor | Pathway | Diploma | CertName | CertPosition | CertDate   |
	| 0              | 0                                     | 0                            | 0                          | 0                 | 0                | 0                | 0              | 0        | 0       | 0       | TestName | TestPosition | 01/08/2013 |
	Then I verify the status for Teachers, Educators, Program, and Enrolments tabs all Read Only	
	And I close the browser successfully	
	Examples:
	| service               |
	| Alexandra & District Kindergarten | 

@annualconfirmation
Scenario Outline: AnnualConfirm_Sessional_Rotational_4yo3yoCombined
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I check the status for Teachers, Educators, Program, and Enrolments tabs all COMPLETE
	Then I verify the status for Annual Confirmation should be COMPLETE
	And I click on annual confirmation tab
	Then I verify the available fields in annual confirmation tab
	And I verify the values in annual confirmation tab
	And I click confirm and submit the data for sessional rotationl program Combined
	| TLDS completed | 0 TLDS completed forwarded to schools | 3 year old children enrolled | no of hours for 3 year old | Total Other Early | Masters or above | Graduate diploma | Dual bachelors | Bachelor | Pathway | Diploma | CertName | CertPosition | CertDate   |
	| 0              | 0                                     | 0                            | 0                          | 0                 | 0                | 0                | 0              | 0        | 0       | 0       | TestName | TestPosition | 01/08/2013 |
	Then I verify the status for Teachers, Educators, Program, and Enrolments tabs all Read Only
	And I close the browser successfully	
	Examples:
	| service                   |
	| Dolena Young Kindergarten |  

@annualconfirmation
Scenario Outline: AnnualConfirm_ViewData
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I click on annual confirmation tab
	And I click View Data button
	And I close the browser successfully
	Examples:
	| service                   |
	| Dolena Young Kindergarten | 
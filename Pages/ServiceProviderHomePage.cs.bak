﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Data;
using System.Threading;
using TechTalk.SpecFlow;
using System.Windows.Forms;
using System.Linq;
using System;
using NUnit.Framework;

namespace KIM_Automation.Pages
{
    class ServiceProviderHomePage : BaseApplicationPage
    {
        public ServiceProviderHomePage(IWebDriver driver) : base(driver)
        { }
        public By AddServiceButton_Element => By.CssSelector("#ctl00_ctl50_g_bac2718a_044a_40e3_8885_d0ee8fdb195a_ctl00_ServiceAddEditButton");
        public IWebElement ServiceName_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_ServiceName']"));
        public IWebElement DateCommence_Element => Driver.FindElement(By.XPath("//button[@id='ctl00_ctl50_g_7cd0e0c1_b447_4440_bd0f_51aadf0767f5_ctl00_ServiceAddEditButton']"));
        public IWebElement UnitNum_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_LocAddressNumber']"));
        public IWebElement StreetNum_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_LocAddressStreetName']"));
        public IWebElement StreetType_Element => Driver.FindElement(By.XPath("//select[@id='ctl01_addSrv_LocAddressStreetType']"));
        public IWebElement Suburb_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_LocAddressSuburb']"));
        public IWebElement State_Element => Driver.FindElement(By.XPath("//select[@id='ctl01_addSrv_LocAddressState']"));
        public IWebElement Lga_Element => Driver.FindElement(By.XPath("//select[@id='ctl02_addSrv_VMASLGACodes']"));
        public IWebElement Postcode_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_LocAddressPostCode']"));
        public IWebElement LgaCode_Element => Driver.FindElement(By.XPath("//input[@id='btnSelectLGA']"));
        public By Tele_Element => By.XPath("//input[@id='addSrv_Telephone']");
        public IWebElement Email_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_Email']"));
        public IWebElement TitleEmergency_Element => Driver.FindElement(By.XPath("//select[@id='ctl01_addSrv_EmergencyContactTitle']"));
        public IWebElement GivenNameEmergency_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_EmergencyContactFirstName']"));
        public IWebElement FamilyNameEmergency_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_EmergencyContactSurName']"));

        internal string FillForm_NoEym(Table table)
        {
            try
            {
                var dataTable = TableExtensions.ToDataTable(table);
                string numRows = dataTable.Rows.ToString();
                foreach (DataRow row in dataTable.Rows)
                {
                   
                    //Storing the name of the new service added
                    NewService = row.ItemArray[0].ToString();
                    //Passing all the elements from the feature file to fill in the form
                    ServiceName_Element.SendKeys(NewService);
                    Thread.Sleep(5000);
                    //Selecting a future date
                    CalendarIcon_Element.Click();
                    CalendarDate_Element.Click();
                    UnitNum_Element.SendKeys(row.ItemArray[2].ToString());
                    Thread.Sleep(5000);
                    StreetNum_Element.SendKeys(row.ItemArray[3].ToString());
                    //Selecting a street type from dropdown
                    var selectElement_street = new SelectElement(StreetType_Element);
                    selectElement_street.SelectByText(row.ItemArray[4].ToString());

                    Suburb_Element.SendKeys(row.ItemArray[5].ToString());
                    //Selecting a state from dropdown
                    var selectElement_state = new SelectElement(State_Element);
                    selectElement_state.SelectByText(row.ItemArray[6].ToString());

                    Postcode_Element.SendKeys(row.ItemArray[7].ToString());
                    State_Element.Click();
                    Thread.Sleep(5000);
                    //Selecting LGA Code
                    LgaCode_Element.Click();
                    Thread.Sleep(3000);
                    LgaRadio_Element.Click();
                    var selectElement_lga = new SelectElement(Lga_Element);
                    selectElement_lga.SelectByText(row.ItemArray[8].ToString());
                    LgaSubmitButton_Element.Click();
                    Thread.Sleep(3000);
                    Driver.FindElement(Tele_Element).SendKeys(row.ItemArray[9].ToString());
                    Thread.Sleep(2000);
                    Email_Element.SendKeys(row.ItemArray[10].ToString());
                    //Selecting emergency title from dropdown
                    var selectElement_title = new SelectElement(TitleEmergency_Element);
                    selectElement_title.SelectByText(row.ItemArray[11].ToString());
                    //Filling emergecy details
                    GivenNameEmergency_Element.SendKeys(row.ItemArray[12].ToString());
                    FamilyNameEmergency_Element.SendKeys(row.ItemArray[13].ToString());
                    Thread.Sleep(3000);
                    EmailEmergency_Element.SendKeys(row.ItemArray[14].ToString());
                    Thread.Sleep(2000);
                    PositionEmergency_Element.SendKeys(row.ItemArray[15].ToString());
                    Thread.Sleep(2000);
                    TeleEmergency_Element.SendKeys(row.ItemArray[16].ToString());
                    Thread.Sleep(2000);
                    MobileEmergency_Element.SendKeys(row.ItemArray[17].ToString());

                    //Selecting council facility radio button
                    SeviceOperatorCouncil_Element.Click();
                    //Forced wait for 3 seconds
                    Thread.Sleep(3000);
                    //Filling in details on next page
                    NextButton_Element.Click();
                    InsuranceCover_Element.Click();
                    Eym_Element.Click();
                    EducationNature_Element.Click();
                    Thread.Sleep(2000);
                    //Selecting quality rating
                    var selectElement_rating = new SelectElement(QualityRating_Element);
                    selectElement_rating.SelectByText(row.ItemArray[18].ToString());
                    ProgramOffered_Element.Click();
                    Thread.Sleep(5000);
                    SubmitButton_Element.Click();
                    Thread.Sleep(5000);
                                       
                }
            }
            catch (System.EntryPointNotFoundException ex)
            {
                System.Console.WriteLine(ex.ToString());

            }
            return NewService;
        }

        internal string FillForm_NoEym_MultipleServices(Table table)
        {
            try
            {
                var dataTable = TableExtensions.ToDataTable(table);
                string numRows = dataTable.Rows.ToString();
                foreach (DataRow row in dataTable.Rows)
                {

                    //Storing the name of the new service added
                    NewService = row.ItemArray[0].ToString();
                    //Passing all the elements from the feature file to fill in the form
                    ServiceName_Element.SendKeys(NewService);
                    Thread.Sleep(5000);
                    //Selecting a future date
                    CalendarIcon_Element.Click();
                    CalendarDate_Element.Click();
                    UnitNum_Element.SendKeys(row.ItemArray[2].ToString());
                    Thread.Sleep(5000);
                    StreetNum_Element.SendKeys(row.ItemArray[3].ToString());
                    //Selecting a street type from dropdown
                    var selectElement_street = new SelectElement(StreetType_Element);
                    selectElement_street.SelectByText(row.ItemArray[4].ToString());

                    Suburb_Element.SendKeys(row.ItemArray[5].ToString());
                    //Selecting a state from dropdown
                    var selectElement_state = new SelectElement(State_Element);
                    selectElement_state.SelectByText(row.ItemArray[6].ToString());

                    Postcode_Element.SendKeys(row.ItemArray[7].ToString());
                    State_Element.Click();
                    Thread.Sleep(5000);
                    //Selecting LGA Code
                    LgaCode_Element.Click();
                    Thread.Sleep(3000);
                    LgaRadio_Element.Click();
                    var selectElement_lga = new SelectElement(Lga_Element);
                    selectElement_lga.SelectByText(row.ItemArray[8].ToString());
                    LgaSubmitButton_Element.Click();
                    Thread.Sleep(3000);
                    Driver.FindElement(Tele_Element).SendKeys(row.ItemArray[9].ToString());
                    Thread.Sleep(2000);
                    Email_Element.SendKeys(row.ItemArray[10].ToString());
                    //Selecting emergency title from dropdown
                    var selectElement_title = new SelectElement(TitleEmergency_Element);
                    selectElement_title.SelectByText(row.ItemArray[11].ToString());
                    //Filling emergecy details
                    GivenNameEmergency_Element.SendKeys(row.ItemArray[12].ToString());
                    FamilyNameEmergency_Element.SendKeys(row.ItemArray[13].ToString());
                    Thread.Sleep(3000);
                    EmailEmergency_Element.SendKeys(row.ItemArray[14].ToString());
                    Thread.Sleep(2000);
                    PositionEmergency_Element.SendKeys(row.ItemArray[15].ToString());
                    Thread.Sleep(2000);
                    TeleEmergency_Element.SendKeys(row.ItemArray[16].ToString());
                    Thread.Sleep(2000);
                    MobileEmergency_Element.SendKeys(row.ItemArray[17].ToString());

                    //Selecting council facility radio button
                    SeviceOperatorCouncil_Element.Click();
                    //Forced wait for 3 seconds
                    Thread.Sleep(3000);
                    //Filling in details on next page
                    NextButton_Element.Click();
                    InsuranceCover_Element.Click();
                    Eym_Element.Click();
                    EducationNature_Element.Click();
                    Thread.Sleep(2000);
                    //Selecting quality rating
                    var selectElement_rating = new SelectElement(QualityRating_Element);
                    selectElement_rating.SelectByText(row.ItemArray[18].ToString());
                    ProgramOffered_Element.Click();
                    Thread.Sleep(5000);
                    SubmitButton_Element.Click();
                    Thread.Sleep(2000);
                    AddService();

                }
            }
            catch (System.EntryPointNotFoundException ex)
            {
                System.Console.WriteLine(ex.ToString());

            }
            return NewService;
        }

        public IWebElement EmailEmergency_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_EmergencyContactEmail']"));
        public IWebElement PositionEmergency_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_EmergencyContactPosition']"));
        public IWebElement TeleEmergency_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_EmergencyContactTelephone']"));
        public IWebElement MobileEmergency_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_EmergencyContactMobile']"));

        internal void AddedSuccess()
        {
            //Thread.Sleep(5000);
            Assert.IsTrue(Driver.FindElement(AddServiceButton_Element).Displayed,"Service not added successfully");
        }

        public IWebElement SeviceOperatorCouncil_Element => Driver.FindElement(By.XPath("//input[@id='rbaddSrvIsOutOfCouncilFacility1']"));
        public IWebElement QualityRating_Element => Driver.FindElement(By.CssSelector("#ctl01_addSrv_NationalQualityFrameWorkRating"));
        public IWebElement ProgramOffered_Element => Driver.FindElement(By.CssSelector("#rbaddSrvIsFundedKindergarten1"));
        public IWebElement NextButton_Element => Driver.FindElement(By.XPath("//button[@name='btnNext']"));
        public IWebElement InsuranceCover_Element => Driver.FindElement(By.CssSelector("#rbaddSrvIsAssociationInc2"));
        public IWebElement Eym_Element => Driver.FindElement(By.CssSelector("#rbaddSrvApplyForCluster2"));
        public IWebElement EducationNature_Element => Driver.FindElement(By.CssSelector("#rbaddSrvNatureOfEducationCare1"));
        public IWebElement SubmitButton_Element => Driver.FindElement(By.XPath("//div[@id='serviceAddEditForm']//input[@name='btnSubmit']"));
        public IWebElement UploadButton_Element => Driver.FindElement(By.CssSelector("#cmUpload"));
        public IWebElement ChooseFile_Element => Driver.FindElement(By.CssSelector("#fileUploadQualifications"));
        public IWebElement CalendarIcon_Element => Driver.FindElement(By.XPath("//div[@class='fullWidth']//span[@class='input-group-addon']"));
        public IWebElement CalendarDate_Element => Driver.FindElement(By.XPath("//td[contains(text(),'31')]"));
        public IWebElement LgaRadio_Element => Driver.FindElement(By.CssSelector("#rbaddSrvVMASOption2"));
        public IWebElement LgaSubmitButton_Element => Driver.FindElement(By.XPath("//div[@id='serviceVMASForm']//input[@name='btnSelect']"));
        public IWebElement UploadButtonFile_Element => Driver.FindElement(By.CssSelector("#btnUploadFile"));
        public IWebElement SaveButtonFile_Element => Driver.FindElement(By.CssSelector("#btnSaveQuals"));
        public IWebElement LeagalCouncil_Element => Driver.FindElement(By.CssSelector("#clusterSrv_rbOptCounsilClusterApproval2"));
        public IWebElement SubmitButton2_Element => Driver.FindElement(By.XPath("//div[@id='clusterApplyFormFooter']//input[@name='btnSubmit']"));
        string NewService;
        //public By SearchBox_Element => By.XPath("//label[contains(text(),'earch')]");
        public By SearchBox_Element => By.XPath("//label[contains(text(),'Search:')]//input");
         //public By SearchBox_Element => By.XPath("//*[@id='serviceFundingSummaryTable_filter']/label/input");
        //public By SearchBox_Element => By.XPath("/html[1]/body[1]/form[1]/div[5]/div[1]/div[1]/div[1]/div[6]/div[2]/span[5]/table[1]/tbody[1]/tr[3]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/label[1]/input[1]");
        //public By NewServiceExists_Element => By.XPath("//td[contains(@class,'sorting_1')]");
        public By NewServiceExists_Element => By.CssSelector("body.ms-backgroundImage:nth-child(2) div.container div.ms-webpart-zone.ms-fullWidth div.s4-wpcell-plain.ms-webpartzone-cell.ms-webpart-cell-vertical.ms-fullWidth div.ms-webpart-chrome.ms-webpart-chrome-vertical.ms-webpart-chrome-fullWidth div.ms-WPBody div.dataTables_wrapper:nth-child(5) table.customTable.serv-list.dataTable tbody:nth-child(2) tr.odd td.sorting_1:nth-child(1) > a:nth-child(1)");
        WebDriverWait waitAgency;
        public IWebElement CloseButton => Driver.FindElement(By.Name("btnClose"));
        public IWebElement AnnualConfTab_Element => Driver.FindElement(By.XPath("//div[@class='tabs datacollectiontabs']//li[5]"));
        public By EditButton_Element => By.XPath("//tr[contains(@class,'odd')]//button[contains(@class,'btn blue edit-apply')]");
        public By RemoveButton_Element => By.CssSelector("#rbDeleteService");
        public By EditButton_Remove => By.XPath("//button[@name='btnEdit']");
        public By RadioButton_Element => By.XPath("//input[@id='remSrv_rbServiceClosing']");
        public By SubmitButton_Remove => By.XPath("//input[@id='btnSubmit']");
        public By ReasonTextBox_Element => By.CssSelector("#remSrv_ceaseReason");
        public By CeaseDate_Element => By.XPath("//input[@id='remSrv_ceasedDate']");
        public IWebElement SelectDate_Today => Driver.FindElement(By.XPath("//td[@class='today active day']"));
        public By EditServiceButton_Element => By.CssSelector("#rbEditService");
        String winHandleBefore;
        internal void AnnualConfirmation()
        {
            Thread.Sleep(5000);
            Assert.IsTrue(AnnualConfTab_Element.Enabled, "Annual confirmation tab is not enabled");
        }

        internal void SearchService(string NewService)
        {

            //waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            //waitAgency.Until(c => c.FindElement(SearchBox_Element));
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            Driver.FindElement(SearchBox_Element).Click();
            Driver.FindElement(SearchBox_Element).SendKeys(NewService);
            //waitAgency.Until(c => c.FindElement(NewServiceExists_Element));
            Driver.FindElement(NewServiceExists_Element).Click();
            
        }

        internal void EditPhone(string telephone)
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(Tele_Element));
            Driver.FindElement(Tele_Element).Clear();
            Driver.FindElement(Tele_Element).SendKeys(telephone);
            SubmitButton_Element.Click();
        }

        internal string EditService(string service)
        {
            winHandleBefore = Driver.CurrentWindowHandle;
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(EditButton_Element));
            Driver.FindElement(EditButton_Element).Click();
            Thread.Sleep(2000);
            waitAgency.Until(c => c.FindElement(EditServiceButton_Element));
            Driver.FindElement(EditServiceButton_Element).Click();
            //Click on edit button to fill more details
            waitAgency.Until(c => c.FindElement(EditButton_Remove));
            Driver.FindElement(EditButton_Remove).Click();
            return winHandleBefore;
        }

        internal void Verify_EditService(string service, string winHandleBefore)
        {

            Driver.SwitchTo().Window(winHandleBefore);

            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(EditButton_Element));
            Driver.FindElement(EditButton_Element).Click();
            Thread.Sleep(2000); 
            Assert.AreEqual(Driver.FindElement(EditServiceButton_Element).Enabled.ToString(), "False");
            
        }

        internal void FindService_EditDelete(string NewService)
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(SearchBox_Element));
           
            Driver.FindElement(SearchBox_Element).Click();
            //Driver.FindElement(SearchBox_Element).Clear();
            Driver.FindElement(SearchBox_Element).SendKeys(NewService);
        }

        internal void AddService()
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(AddServiceButton_Element));
            Driver.FindElement(AddServiceButton_Element).Click();
            Thread.Sleep(3000);
        }

        public String FillForm(Table table) 
        {
            try
            {
                var dataTable = TableExtensions.ToDataTable(table);
                foreach (DataRow row in dataTable.Rows)
                {
                    //Storing the name of the new service added
                    NewService = row.ItemArray[0].ToString();
                    //Passing all the elements from the feature file to fill in the form
                    ServiceName_Element.SendKeys(NewService);
                    Thread.Sleep(5000);
                    //Selecting a future date
                    CalendarIcon_Element.Click();
                    CalendarDate_Element.Click();
                    UnitNum_Element.SendKeys(row.ItemArray[2].ToString());
                    Thread.Sleep(2000);
                    StreetNum_Element.SendKeys(row.ItemArray[3].ToString());
                    //Selecting a street type from dropdown
                    var selectElement_street = new SelectElement(StreetType_Element);
                    selectElement_street.SelectByText(row.ItemArray[4].ToString());

                    Suburb_Element.SendKeys(row.ItemArray[5].ToString());
                    //Selecting a state from dropdown
                    var selectElement_state = new SelectElement(State_Element);
                    selectElement_state.SelectByText(row.ItemArray[6].ToString());

                    Postcode_Element.SendKeys(row.ItemArray[7].ToString());
                    State_Element.Click();
                    Thread.Sleep(5000);
                    //Selecting LGA Code
                    LgaCode_Element.Click();
                    Thread.Sleep(3000);
                    LgaRadio_Element.Click();
                    var selectElement_lga = new SelectElement(Lga_Element);
                    selectElement_lga.SelectByText(row.ItemArray[8].ToString());
                    LgaSubmitButton_Element.Click();
                    Thread.Sleep(3000);
                    Driver.FindElement(Tele_Element).SendKeys(row.ItemArray[9].ToString());
                    Thread.Sleep(2000);
                    Email_Element.SendKeys(row.ItemArray[10].ToString());
                    //Selecting emergency title from dropdown
                    var selectElement_title = new SelectElement(TitleEmergency_Element);
                    selectElement_title.SelectByText(row.ItemArray[11].ToString());
                    //Filling emergecy details
                    GivenNameEmergency_Element.SendKeys(row.ItemArray[12].ToString());
                    FamilyNameEmergency_Element.SendKeys(row.ItemArray[13].ToString());
                    Thread.Sleep(3000);
                    EmailEmergency_Element.SendKeys(row.ItemArray[14].ToString());
                    Thread.Sleep(2000);
                    PositionEmergency_Element.SendKeys(row.ItemArray[15].ToString());
                    Thread.Sleep(2000);
                    TeleEmergency_Element.SendKeys(row.ItemArray[16].ToString());
                    Thread.Sleep(2000);
                    MobileEmergency_Element.SendKeys(row.ItemArray[17].ToString());
                    
                    //Selecting council facility radio button
                    SeviceOperatorCouncil_Element.Click();
                    //Forced wait for 3 seconds
                    Thread.Sleep(3000);
                    //Filling in details on next page
                    NextButton_Element.Click();
                    InsuranceCover_Element.Click();
                    Eym_Element.Click();
                    EducationNature_Element.Click();
                    Thread.Sleep(2000);
                    //Selecting quality rating
                    var selectElement_rating = new SelectElement(QualityRating_Element);
                    selectElement_rating.SelectByText(row.ItemArray[18].ToString());
                    ProgramOffered_Element.Click();
                    Thread.Sleep(5000);
                    SubmitButton_Element.Click();
                    Thread.Sleep(5000);
                    var winHandleBefore = Driver.CurrentWindowHandle;

                    UploadButton_Element.Click();
                    Thread.Sleep(2000);
                    //Switch to upload window
                    Driver.SwitchTo().Window(Driver.WindowHandles.Last());
                    //Uploading the file using keyboard actions
                    Thread.Sleep(5000);
                    ChooseFile_Element.Click();
                    Thread.Sleep(5000);
                    var fileNamePath = "C:\\Users\\09946103\\Documents\\KeyValue_Table.txt";
                    SendKeys.SendWait(fileNamePath);
                    SendKeys.SendWait(@"{Enter}");
                    Thread.Sleep(5000);
                    UploadButtonFile_Element.Click();
                    SaveButtonFile_Element.Click();
                    //Brings the control back to original window
                    Driver.SwitchTo().Window(winHandleBefore);
                    
                }
             }
             catch (System.EntryPointNotFoundException ex)
             {
                 System.Console.WriteLine(ex.ToString());
                
            }
            return NewService;
        }


        internal void DeleteService(string service)
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(EditButton_Element));
           
            Driver.FindElement(EditButton_Element).Click();
            Thread.Sleep(2000);
            waitAgency.Until(c => c.FindElement(RemoveButton_Element));
            Driver.FindElement(RemoveButton_Element).Click();
            //Click on edit button to fill more details
            waitAgency.Until(c => c.FindElement(EditButton_Remove));
            Driver.FindElement(EditButton_Remove).Click();
        }
        internal void ConfirmDeleteService(string reason)
        {
            Thread.Sleep(2000);
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(RadioButton_Element));
            Driver.FindElement(RadioButton_Element).Click();
            waitAgency.Until(c => c.FindElement(CeaseDate_Element));
            Driver.FindElement(CeaseDate_Element).Click();
            SelectDate_Today.Click();
            waitAgency.Until(c => c.FindElement(ReasonTextBox_Element));
            Driver.FindElement(ReasonTextBox_Element).Click();
            Driver.FindElement(ReasonTextBox_Element).SendKeys(reason);
            //Click on submit button to submit the request and close the form 
            waitAgency.Until(c => c.FindElement(SubmitButton_Remove));
            Driver.FindElement(SubmitButton_Remove).Click();  
        }
        internal void SubmitForm()
        {
            LeagalCouncil_Element.Click();
            Thread.Sleep(2000);
            SubmitButton2_Element.Click();
            Thread.Sleep(2000);
        }
        
    }
 }


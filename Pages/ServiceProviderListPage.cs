﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace KIM_Automation.Pages
{
    class ServiceProviderListPage : BaseApplicationPage
    {
        public By Search_Element => By.XPath("//label[contains(text(),'Search:')]//input");
        public IWebElement SelectSP_Element => Driver.FindElement(By.XPath("//tr[1]//td[2]//a[1]"));
        public ServiceProviderListPage(IWebDriver driver) : base(driver)
        { }

        internal void SearchAndClick()
        {
            string organisation = CommonFunctions.GetResourceValue("ServiceProvider");
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Search_Element));
            Driver.FindElement(Search_Element).SendKeys(organisation);
            SelectSP_Element.Click();
        }
    }
}

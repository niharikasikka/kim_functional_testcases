﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace KIM_Automation.Pages
{
    class ESKEnrolmentsPage : BaseApplicationPage
    {
        public ESKEnrolmentsPage(IWebDriver driver) : base(driver)
        { }

        public By EarlyStartKindergarten_Element => By.XPath("//input[@class='btn blue float-left']");
        public By ESKEnrolment_Element => By.XPath("//strong[contains(text(),'ESK Enrolment')]");
        public By AddNewESKEnrolment_Element => By.XPath("//input[@class='btn blue add-eskenrol float-right']");

        public By GivenName_Element => By.CssSelector("#enrolDetailsGName");
        public By FamilyName_Element => By.CssSelector("#enrolDetailsFName");
        //Selecting a Gender type from dropdown
        public By Gender_dropdown => By.CssSelector("#enrolOptGender");
        public By DateOfBirth_Element => By.CssSelector("#enrolDetailsDOB");
        public By UnitNum_Element => By.XPath("//input[@id='enrolNumber']");
        public By StreetNum_Element => By.XPath("//input[@id='enrolStreet']");
        public IWebElement StreetType_Element => Driver.FindElement(By.XPath("//select[@id='enrolStreetType']"));
        public By Suburb_Element => By.XPath("//input[@id='enrolSuburb']");
        public IWebElement State_Element => Driver.FindElement(By.XPath("//select[@id='enrolState']"));
        public By Postcode_Element => By.XPath("//input[@id='enrolPostcode']");

        //Child Frist 
        public By ChildFirst_Element => By.XPath("//input[@id='commencedDate']");

        //Early start dropdown

        public IWebElement EarlyStart_dropdown => Driver.FindElement(By.CssSelector("#eskEligibility"));

        public By ChildEligible_Checkbox => By.CssSelector("#isKnownToChildProtection");

        //Early Start radio btn

        public By EarlyStartRadioBtn_Element => By.XPath("//input[@id='programChildIn_3Years']");

        //Early Childhood Teacher dropdown

        public IWebElement EarlyChildhoodTeacher_dropdown => Driver.FindElement(By.CssSelector("#teacherID"));

        public By AEL_Element => By.CssSelector("#accessToAEL_No");

        //NextBtn
        public By NextBtn_Element => By.XPath("//form[@id='eskEnrolmentAddEditForm']//button[@name='btnNext'][contains(text(),'Next')]");

        //ChildLive dropdown

        public IWebElement ChildLive => Driver.FindElement(By.XPath("//span[@class='dropdownlisttext']"));

        public IWebElement ChildLive_Selection => Driver.FindElement(By.XPath("//li[contains(text(),'Parent/s')]"));


        //Key Age radio btn

        public By KeyAge_Radiobtn => By.CssSelector("#isKASMCHVisitUptoDate_Yes");

        //Permission RadioBtn

        public By Permission_Radiobtn => By.CssSelector("#hasPermission_Yes");

        //KIS_RadioBtn

        public By KIS_Radiobtn => By.CssSelector("#isKIS_Yes");

        //SPPI Previous Year

        public By SPPIPrevious_Radiobtn => By.CssSelector("#isAttendedSPPIPreviousYear_Yes");

        //SPPI_RadioBtn

        public By SPPICurrent_Radiobtn => By.CssSelector("#isAttendingSPPIYes");

        //AdditionalNeeds_RadioBtn
        public By AdditionalNeeds_Radiobtn => By.CssSelector("#isAdditionalNeeds_No");
        public By ChildEnrolledHours_Element => By.CssSelector("#hours");
        public By ChildEnrolledMinutes_Element => By.CssSelector("#minutes");
        public By ChildEnrolledWeeks_Element => By.CssSelector("#weeks");
        //IntegratedLongdaycare_RadioBtn
        public By IntegratedLongDaycare_Radiobtn => By.CssSelector("#isProgramIntegrated_No");
        //ImmunisationStatus dropdown
        public IWebElement ImmunisationStatus_dropdown => Driver.FindElement(By.XPath("//select[@id='immunisationStatus']"));
        //Submit Btn
        public By Submit_Btn => By.XPath("//form[@id='eskEnrolmentAddEditForm']//input[@name='btnSubmit']");
        public By Search_ESKEnrolment => By.XPath("//div[@id='DataTables_Table_1_filter']//input");
        public By Edit_ESKEnrolment => By.XPath("//tbody[@id='ESKEnrolmentSummaryDataGrid']//tr[contains(@class,'odd')]//button[@class='btn blue eskedit'][contains(text(),'Edit')]");
        public By EnrolmentTabStatusElement => By.XPath("//span[@class='status enrolment-tab-status']");

        internal void FillNewESKEnrolmentForm(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {

                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
                wait.Until(x => x.FindElement(GivenName_Element));
                //random generator of GivenName
                Random generatorGn = new Random();
                int iGn = generatorGn.Next(1, 10000000);
                string sGn = iGn.ToString().PadLeft(7, '0');
                string eGivenName = "EGN." + sGn;
                //removed Driver.FindElement(GivenName_Element).SendKeys(row.ItemArray[0].ToString());
                Driver.FindElement(GivenName_Element).SendKeys(eGivenName);

                //random generator of FamilyName
                Random generatorFn = new Random();
                int iFn = generatorFn.Next(1, 10000000);
                string sFn = iFn.ToString().PadLeft(7, '0');
                string eFamilyName = "Fn." + sFn;
                //removed Driver.FindElement(FamilyName_Element).SendKeys(row.ItemArray[1].ToString());
                Driver.FindElement(FamilyName_Element).SendKeys(eFamilyName);

                //Wait for gender dropdown to be visible and then select the value
                wait.Until(x => x.FindElement(Gender_dropdown));
                SelectElement Gender = new SelectElement(Driver.FindElement(Gender_dropdown));
                Gender.SelectByText(row.ItemArray[2].ToString());

                wait.Until(x => x.FindElement(DateOfBirth_Element));
                //random generator DOB
                DateTime start = new DateTime((DateTime.Now.Year - 5), 1, 1);
                DateTime end = new DateTime((DateTime.Now.Year - 5), 12, 31);
                Random gen = new Random();
                int range = ((TimeSpan)(end - start)).Days;
                string dOB = start.AddDays(gen.Next(range)).ToString("dd/MM/yyyy");
                //removed Driver.FindElement(DateOfBirth_Element).SendKeys(row.ItemArray[3].ToString());
                Driver.FindElement(DateOfBirth_Element).SendKeys(dOB);

                wait.Until(x => x.FindElement(UnitNum_Element));
                //random generator of Unit number
                Random generatorUn = new Random();
                int iUn = generatorUn.Next(1, 1000000);
                string unitNum = iUn.ToString().PadLeft(6, '0');
                //removed Driver.FindElement(UnitNum_Element).SendKeys(row.ItemArray[4].ToString());
                Driver.FindElement(UnitNum_Element).SendKeys(unitNum);

                wait.Until(x => x.FindElement(StreetNum_Element));
                //random generator of Street
                Random generatorStn = new Random();
                int iStn = generatorStn.Next(1, 1000000);
                string sStn = iStn.ToString().PadLeft(6, '0');
                string stnum = "Str-" + DateTime.Now.ToString("ddmmyyyy") + "_" + sStn;
                //removedDriver.FindElement(StreetNum_Element).SendKeys(row.ItemArray[5].ToString());
                Driver.FindElement(StreetNum_Element).SendKeys(stnum);

                SelectElement StreetType_Dropdown = new SelectElement(StreetType_Element);
                StreetType_Dropdown.SelectByText(row.ItemArray[6].ToString());

                wait.Until(x => x.FindElement(Suburb_Element));
                Driver.FindElement(Suburb_Element).SendKeys(row.ItemArray[7].ToString());

                SelectElement State = new SelectElement(State_Element);
                State.SelectByText(row.ItemArray[8].ToString());

                wait.Until(x => x.FindElement(Postcode_Element));
                //random generator of Post Code
                Random generatorPc = new Random();
                int iPc = generatorPc.Next(1, 1000);
                string postCode = iPc.ToString().PadLeft(3, '0');
                //removed Driver.FindElement(Postcode_Element).SendKeys(row.ItemArray[9].ToString());
                Driver.FindElement(Postcode_Element).SendKeys(postCode);

                wait.Until(x => x.FindElement(ChildFirst_Element));
                Driver.FindElement(ChildFirst_Element).SendKeys(row.ItemArray[10].ToString());

                SelectElement EarlyStart = new SelectElement(EarlyStart_dropdown);
                EarlyStart.SelectByText(row.ItemArray[11].ToString());

                wait.Until(x => x.FindElement(ChildEligible_Checkbox));
                Driver.FindElement(ChildEligible_Checkbox).Click();

                wait.Until(x => x.FindElement(EarlyStartRadioBtn_Element));
                Driver.FindElement(EarlyStartRadioBtn_Element).Click();

                SelectElement EarlyChildhoodTeacher = new SelectElement(EarlyChildhoodTeacher_dropdown);
                EarlyChildhoodTeacher.SelectByText(row.ItemArray[12].ToString());

                wait.Until(x => x.FindElement(AEL_Element));
                Driver.FindElement(AEL_Element).Click();

                wait.Until(x => x.FindElement(NextBtn_Element));
                Driver.FindElement(NextBtn_Element).Click();

                ChildLive.Click();
                ChildLive_Selection.Click();

                wait.Until(x => x.FindElement(KeyAge_Radiobtn));
                Driver.FindElement(KeyAge_Radiobtn).Click();

                wait.Until(x => x.FindElement(Permission_Radiobtn));
                Driver.FindElement(Permission_Radiobtn).Click();

                wait.Until(x => x.FindElement(KIS_Radiobtn));
                Driver.FindElement(KIS_Radiobtn).Click();

                wait.Until(x => x.FindElement(SPPIPrevious_Radiobtn));
                Driver.FindElement(SPPIPrevious_Radiobtn).Click();

                wait.Until(x => x.FindElement(SPPICurrent_Radiobtn));
                Driver.FindElement(SPPICurrent_Radiobtn).Click();

                wait.Until(x => x.FindElement(SPPICurrent_Radiobtn));
                Driver.FindElement(SPPICurrent_Radiobtn).Click();

                wait.Until(x => x.FindElement(AdditionalNeeds_Radiobtn));
                Driver.FindElement(AdditionalNeeds_Radiobtn).Click();

                wait.Until(x => x.FindElement(ChildEnrolledHours_Element));
                Driver.FindElement(ChildEnrolledHours_Element).SendKeys(row.ItemArray[13].ToString());

                wait.Until(x => x.FindElement(ChildEnrolledMinutes_Element));
                Driver.FindElement(ChildEnrolledMinutes_Element).SendKeys(row.ItemArray[14].ToString());

                wait.Until(x => x.FindElement(ChildEnrolledWeeks_Element));
                Driver.FindElement(ChildEnrolledWeeks_Element).SendKeys(row.ItemArray[15].ToString());

                wait.Until(x => x.FindElement(IntegratedLongDaycare_Radiobtn));
                Driver.FindElement(IntegratedLongDaycare_Radiobtn).Click();

                SelectElement ImmunisationStatus = new SelectElement(ImmunisationStatus_dropdown);
                ImmunisationStatus.SelectByText(row.ItemArray[16].ToString());

                wait.Until(x => x.FindElement(Submit_Btn));
                Driver.FindElement(Submit_Btn).Click();
                //Wait for form to close
                Task.Delay(5000).Wait();
                wait.Until(x => x.FindElement(AddNewESKEnrolment_Element));
                Driver.FindElement(AddNewESKEnrolment_Element).Click();

            }
        }
        internal void AddNewESKEnrolments()
        {

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.FindElement(EarlyStartKindergarten_Element).Click();

            wait.Until(x => x.FindElement(ESKEnrolment_Element));
            Driver.FindElement(ESKEnrolment_Element).Click();

            wait.Until(x => x.FindElement(AddNewESKEnrolment_Element));
            Driver.FindElement(AddNewESKEnrolment_Element).Click();

        }

        internal void EditESKEnrolment(string SLK)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.FindElement(EarlyStartKindergarten_Element).Click();

            wait.Until(x => x.FindElement(ESKEnrolment_Element));
            Driver.FindElement(ESKEnrolment_Element).Click();
            Driver.FindElement(Search_ESKEnrolment).SendKeys(SLK);
            //Clicking on edit button
            wait.Until(x => x.FindElement(Edit_ESKEnrolment));
            Driver.FindElement(Edit_ESKEnrolment).Click();
        }

        internal void EditStreet(string Street)
        {
            //Updating the Suburb of the child
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            Driver.FindElement(StreetNum_Element).SendKeys(Street);
        }

        /*This method checks the status of the tab
         * If the enrolment tab has completed enrolments 
         * and 
         * confirmed enrolments the status should be completed
         * Else this method will throw an exception and fail the test
         */
        internal void EskEnrolmentStatus()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            //verify Annual Tab status
            wait.Until(x => x.FindElement(EnrolmentTabStatusElement));
            string strEnrolTabStatus = Driver.FindElement(EnrolmentTabStatusElement).Text;
            if (strEnrolTabStatus.Equals("Complete"))
            {

            }
            else
            {
                throw new Exception("Fail: Enrolment tab status is " + strEnrolTabStatus);
            };
        }
    }
}
﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;


namespace KIM_Automation.Pages
{

    class ExportToExcelPage : BaseApplicationPage
    {
        //ESK Export to Excel Elements
        public ExportToExcelPage(IWebDriver driver) : base(driver)
        { }
        public By EarlyStartKindergarten_Element => By.XPath("//input[@class='btn blue float-left']");
        public By ExportToExcel_ESKTeachers => By.XPath("//a[@class='btn blue pull-right']");

        public By ESKEnrolment_Element => By.XPath("//strong[contains(text(),'ESK Enrolment')]");
        public By ExportToExcel_ESKEnrolments => By.XPath("//a[@id='exportESKEnrolmentToExcel']");

        //YBSK Export to Excel Elements
        public By YBSKTeachersTab_Element => By.CssSelector("a[href='#tab1']");
        public By ExportToExcel_YBSKTeachers => By.XPath("//div[@id='tab1']//a[@class='btn blue pull-right'][contains(text(),'Export to Excel')]");
        public By YBSKOtherEducatorsTab_Element => By.CssSelector("a[href='#tab2']");
        public By ExportToExcel_YBSKEducators => By.XPath("//div[@id='tab2']//a[@class='btn blue pull-right'][contains(text(),'Export to Excel')]");
        public By YBSKProgramsTab_Element => By.CssSelector("a[href='#tab3']");
        public By ExportToExcel_YBSKPrograms => By.XPath("//div[@id='tab3']//a[@class='btn blue'][contains(text(),'Export to Excel')]");
        public By YBSKEnrolmentsTab_Element => By.CssSelector("a[href='#tab4']");
        public By ExportToExcel_YBSKEnrolments => By.CssSelector("#exportEnrolmentToExcel");
        internal void ExportToExcel()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.FindElement(EarlyStartKindergarten_Element));
            Driver.FindElement(EarlyStartKindergarten_Element).Click();

            wait.Until(x => x.FindElement(ExportToExcel_ESKTeachers));
            Driver.FindElement(ExportToExcel_ESKTeachers).Click();
            //Thread.Sleep(5000);

            wait.Until(x => x.FindElement(ESKEnrolment_Element));
            Driver.FindElement(ESKEnrolment_Element).Click();

            wait.Until(x => x.FindElement(ExportToExcel_ESKEnrolments));
            Driver.FindElement(ExportToExcel_ESKEnrolments).Click();

        }
        internal void YBSK_ExportToExcel_Teachers()
        {
            //Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            wait.Until(x => x.FindElement(YBSKTeachersTab_Element));
            Driver.FindElement(YBSKTeachersTab_Element).Click();

            wait.Until(x => x.FindElement(ExportToExcel_YBSKTeachers));
            Driver.FindElement(ExportToExcel_YBSKTeachers).Click();
            //Thread.Sleep(10000);            
            wait.Until(x => x.FindElement(YBSKTeachersTab_Element).Displayed);
            Task.Delay(15000).Wait();

            //verify if export to excel is correctly downloaded            
            string DLdir = CommonFunctions.GetResourceValue("DownloadDir");
            String[] allFile = Directory.GetFiles(DLdir, "teacher*");
            String lastFile = allFile[allFile.Length - 1];

            if (Directory.Exists(DLdir))
            {
                if (File.Exists(lastFile))
                {
                    /*FileStream fs = new FileStream("C:\\Users\\09976245\\Documents\\Test.txt", FileMode.Create);
                    TextWriter tmp = Console.Out;
                    StreamWriter sw = new StreamWriter(fs);
                    Console.SetOut(sw);
                    Console.WriteLine("File 1 " + lastFile);
                    Console.SetOut(tmp);
                    sw.Close();*/
                }
                else
                {
                    throw new Exception("Export file for teacher doesn't exist");
                };
            }
            else
            {
                throw new Exception("Directory " + DLdir + " doesn't exist");
            }
        }
        internal void YBSK_ExportToExcel_Educators()
        {
            //Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.FindElement(YBSKOtherEducatorsTab_Element));
            Driver.FindElement(YBSKOtherEducatorsTab_Element).Click();

            wait.Until(x => x.FindElement(ExportToExcel_YBSKEducators));
            Driver.FindElement(ExportToExcel_YBSKEducators).Click();
            //Thread.Sleep(10000);
            wait.Until(x => x.FindElement(YBSKOtherEducatorsTab_Element).Displayed);
            Task.Delay(20000).Wait();

            //verify if export to excel is correctly downloaded            
            string DLdir = CommonFunctions.GetResourceValue("DownloadDir");
            String[] allFile = Directory.GetFiles(DLdir, "coeducator*");
            String lastFile = allFile[allFile.Length - 1];

            if (Directory.Exists(DLdir))
            {
                if (File.Exists(lastFile))
                {
                    /*FileStream fs = new FileStream("C:\\Users\\09976245\\Documents\\Test.txt", FileMode.Create);
                    TextWriter tmp = Console.Out;
                    StreamWriter sw = new StreamWriter(fs);
                    Console.SetOut(sw);
                    Console.WriteLine("File 1 " + lastFile);
                    Console.SetOut(tmp);
                    sw.Close();*/
                }
                else
                {
                    throw new Exception("Export file for other educator doesn't exist");
                };
            }
            else
            {
                throw new Exception("Directory " + DLdir + " doesn't exist");
            }
        }
        internal void YBSK_ExportToExcel_Programs()
        {
            //Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.FindElement(YBSKProgramsTab_Element));
            Driver.FindElement(YBSKProgramsTab_Element).Click();

            wait.Until(x => x.FindElement(ExportToExcel_YBSKPrograms));
            Driver.FindElement(ExportToExcel_YBSKPrograms).Click();
            //Thread.Sleep(10000);
            wait.Until(x => x.FindElement(YBSKProgramsTab_Element).Displayed);
            Task.Delay(15000).Wait();

            //verify if export to excel is correctly downloaded            
            string DLdir = CommonFunctions.GetResourceValue("DownloadDir");
            String[] allFile = Directory.GetFiles(DLdir, "program*");
            String lastFile = allFile[allFile.Length - 1];

            if (Directory.Exists(DLdir))
            {
                if (File.Exists(lastFile))
                {
                    /*FileStream fs = new FileStream("C:\\Users\\09976245\\Documents\\Test.txt", FileMode.Create);
                    TextWriter tmp = Console.Out;
                    StreamWriter sw = new StreamWriter(fs);
                    Console.SetOut(sw);
                    Console.WriteLine("File 1 " + lastFile);
                    Console.SetOut(tmp);
                    sw.Close();*/
                }
                else
                {
                    throw new Exception("Export file for program doesn't exist");
                };
            }
            else
            {
                throw new Exception("Directory " + DLdir + " doesn't exist");
            }
        }
        internal void YBSK_ExportToExcel_Enrolments()
        {
            //Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.FindElement(YBSKEnrolmentsTab_Element));
            Driver.FindElement(YBSKEnrolmentsTab_Element).Click();

            wait.Until(x => x.FindElement(ExportToExcel_YBSKEnrolments));
            Driver.FindElement(ExportToExcel_YBSKEnrolments).Click();
            //Thread.Sleep(10000);
            wait.Until(x => x.FindElement(YBSKEnrolmentsTab_Element).Displayed);
            Task.Delay(20000).Wait();

            //verify if export to excel is correctly downloaded            
            string DLdir = CommonFunctions.GetResourceValue("DownloadDir");
            String[] allFile = Directory.GetFiles(DLdir, "enrolment*");
            String lastFile = allFile[allFile.Length - 1];

            if (Directory.Exists(DLdir))
            {
                if (File.Exists(lastFile))
                {
                    /*FileStream fs = new FileStream("C:\\Users\\09976245\\Documents\\Test.txt", FileMode.Create);
                    TextWriter tmp = Console.Out;
                    StreamWriter sw = new StreamWriter(fs);
                    Console.SetOut(sw);
                    Console.WriteLine("File 1 " + lastFile);
                    Console.SetOut(tmp);
                    sw.Close();*/
                }
                else
                {
                    throw new Exception("Export file for enrolment doesn't exist");
                };
            }
            else
            {
                throw new Exception("Directory " + DLdir + " doesn't exist");
            }
        }

    }
}

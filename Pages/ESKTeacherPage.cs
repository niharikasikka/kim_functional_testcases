﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Data;
using System.Collections.Generic;
using System.Threading;
using TechTalk.SpecFlow;

namespace KIM_Automation.Pages
{
    class ESKTeacherPage : BaseApplicationPage
    {
        public ESKTeacherPage(IWebDriver driver) : base(driver)
        { }

        public By EarlyStartKindergarten_Element => By.XPath("//input[@class='btn blue float-left']");
        public By AddNewESKTeacher_Element => By.XPath("//input[@class='btn blue add-eskteacher']");

        public By GivenName_Element => By.CssSelector("#txtDetailsGName");
        public By FamilyName_Element => By.CssSelector("#txtDetailsFName");
        //Selecting a Gender type from dropdown
        public By Gender_dropdown => By.CssSelector("#ddlOptGender");
        public By DateOfBirth_Element => By.CssSelector("#txtDetailsDOB");
        public By CommencementDate_Element => By.CssSelector("#txtDateCommenced");
        //Teachers Registration status dropdown
        public IWebElement TeachersRegStatus_dropdown => Driver.FindElement(By.CssSelector("#ddlOptVITRegStatus"));
        public By VITNum_Element => (By.CssSelector("#txtVITNumber"));
        public By QualificationDetails_radiobtn => (By.CssSelector("#rbOptTeacherQualsApplied1"));
        //University Dropdown
        public IWebElement University_dropdown => Driver.FindElement(By.CssSelector("#ddlLstUniversity"));
        //Course dropdown 
        public IWebElement Course_dropdown => Driver.FindElement(By.CssSelector("#ddlLstCourse"));

        public By Yearawarded_Element => By.CssSelector("#intAusYearAwarded");

        //Selecting the check box
        public By Qualification_checkbox => (By.CssSelector("#chkAusQualificationHardCopy"));

        public By FourYearQualification_Radiobtn => (By.CssSelector("#opt3or4Year2"));

        //Save btn

        public By SaveBtn_Element => By.XPath("//form[@id='eskTeacherAddEditForm']//input[@name='btnSave']");

        //Search Teacher
        public By SearchESKTeacher_Element => By.XPath("//label[contains(text(),'Search:')]//input");

        //Edit Teacher
        public By EditESKTeacher_Element => By.XPath("//tr[1]//td[5]//span[1]//button[1]");

        //Remove Teacher

        public By RemoveESKTeacher_Element => By.XPath("//tr[1]//td[5]//span[1]//button[2]");

        public By FinalDate_ElementIcon => By.CssSelector("#dtDepartureDate");

        public By FinalDate_Select => (By.XPath("//td[@class='today active day']"));

        public IWebElement Dropdown_Element => Driver.FindElement(By.CssSelector("#ddlReasonLeaving"));

        public By RemoveButton_ESKTeacher => (By.XPath("//form[@id='eskTeacherDeleteForm']//input[contains(@class,'btn blue btn-primary delete submit')]"));
        public By ShowInactiveTeacherButton_ESKTeacher => (By.XPath("//button[@class='btn blue eskteacher-active-toggle pull-left active-state']"));
        public By ESKTeachersTabNoofInactiveTeacherElement => (By.CssSelector("#DataTables_Table_0_info"));
        public By ShowActiveTeacherButton_ESKTeacher => (By.XPath("//button[@class='btn blue eskteacher-active-toggle pull-left inactive-state']"));
        public By TeacherTabStatusElement => By.XPath("//span[@class='status teacher-tab-status']");

        internal void FillESKTeacherForm(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {

                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(60));
                wait.Until(x => x.FindElement(GivenName_Element));
                Driver.FindElement(GivenName_Element).SendKeys(row.ItemArray[0].ToString());

                wait.Until(x => x.FindElement(FamilyName_Element));
                Driver.FindElement(FamilyName_Element).SendKeys(row.ItemArray[1].ToString());

                //Wait for gender dropdown to be visible and then select the value
                wait.Until(x => x.FindElement(Gender_dropdown));
                SelectElement Gender = new SelectElement(Driver.FindElement(Gender_dropdown));
                Gender.SelectByText(row.ItemArray[2].ToString());

                wait.Until(x => x.FindElement(DateOfBirth_Element));
                Driver.FindElement(DateOfBirth_Element).SendKeys(row.ItemArray[3].ToString());

                wait.Until(x => x.FindElement(CommencementDate_Element));
                Driver.FindElement(CommencementDate_Element).SendKeys(row.ItemArray[4].ToString());

                SelectElement RegStatus = new SelectElement(TeachersRegStatus_dropdown);
                RegStatus.SelectByText(row.ItemArray[5].ToString());

                wait.Until(x => x.FindElement(VITNum_Element));
                Driver.FindElement(VITNum_Element).SendKeys(row.ItemArray[6].ToString());

                wait.Until(x => x.FindElement(QualificationDetails_radiobtn));
                Driver.FindElement(QualificationDetails_radiobtn).Click();

                SelectElement University = new SelectElement(University_dropdown);
                University.SelectByText(row.ItemArray[7].ToString());

                SelectElement Course = new SelectElement(Course_dropdown);
                Course.SelectByText(row.ItemArray[8].ToString());

                wait.Until(x => x.FindElement(Yearawarded_Element));
                Driver.FindElement(Yearawarded_Element).Clear();
                Driver.FindElement(Yearawarded_Element).SendKeys(row.ItemArray[9].ToString());

                wait.Until(x => x.FindElement(Qualification_checkbox));
                Driver.FindElement(Qualification_checkbox).Click();

                wait.Until(x => x.FindElement(FourYearQualification_Radiobtn));
                Driver.FindElement(FourYearQualification_Radiobtn).Click();

                wait.Until(x => x.FindElement(SaveBtn_Element));
                Driver.FindElement(SaveBtn_Element).Click();

                wait.Until(x => x.FindElement(AddNewESKTeacher_Element).Displayed);
                //Driver.FindElement(AddNewESKTeacher_Element).Click();
            }
        }
        internal void AddNewESKTeacher()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            Driver.FindElement(EarlyStartKindergarten_Element).Click();

            wait.Until(x => x.FindElement(AddNewESKTeacher_Element));
            Driver.FindElement(AddNewESKTeacher_Element).Click();

        }

        internal void EditESKTeacher(string ESKTeacher)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.FindElement(EarlyStartKindergarten_Element).Click();
            wait.Until(x => x.FindElement(SearchESKTeacher_Element));
            Driver.FindElement(SearchESKTeacher_Element).Click();
            Driver.FindElement(SearchESKTeacher_Element).SendKeys(ESKTeacher);
            //Clicking on edit button
            wait.Until(x => x.FindElement(EditESKTeacher_Element));
            Driver.FindElement(EditESKTeacher_Element).Click();
        }

        internal void EditCommenceDate(string commenceDate)
        {
            //Updating the commence date of the teacher
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            Driver.FindElement(CommencementDate_Element).SendKeys(commenceDate);
        }

        internal void EditYearAwarded(string yearawarded)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            Driver.FindElement(Yearawarded_Element).Clear();
            Driver.FindElement(Yearawarded_Element).SendKeys(yearawarded);

            wait.Until(x => x.FindElement(SaveBtn_Element));
            Driver.FindElement(SaveBtn_Element).Click();

        }


        internal void DeleteESKTeacher(string ESKTeacher)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.FindElement(EarlyStartKindergarten_Element).Click();
            wait.Until(x => x.FindElement(SearchESKTeacher_Element));
            Driver.FindElement(SearchESKTeacher_Element).Click();
            Driver.FindElement(SearchESKTeacher_Element).SendKeys(ESKTeacher);

            //Clicking on Remove button
            wait.Until(x => x.FindElement(RemoveESKTeacher_Element));
            Driver.FindElement(RemoveESKTeacher_Element).Click();

        }

        internal void ConfirmRemove_ESKTeacher(string reason)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(FinalDate_ElementIcon));
            //Select final date from date picker

            wait.Until(x => x.FindElement(FinalDate_ElementIcon));
            Driver.FindElement(FinalDate_ElementIcon).Click();

            //Select reason for leaving from dropdown
            var selectElement_reason = new SelectElement(Dropdown_Element);
            selectElement_reason.SelectByText(reason);

            wait.Until(x => x.FindElement(RemoveButton_ESKTeacher));
            Driver.FindElement(RemoveButton_ESKTeacher).Click();
        }

        internal void VerifyDelete_ESKTeacher(string ESKTeacher)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(SearchESKTeacher_Element));
            Driver.FindElement(SearchESKTeacher_Element).Click();
            Driver.FindElement(SearchESKTeacher_Element).SendKeys(ESKTeacher);


        }
        internal void MakeInactiveESKTeacherActive()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(EarlyStartKindergarten_Element));
            Driver.FindElement(EarlyStartKindergarten_Element).Click();                        
            wait.Until(x => x.FindElement(ShowInactiveTeacherButton_ESKTeacher));
            Driver.FindElement(ShowInactiveTeacherButton_ESKTeacher).Click();
            char[] charsToTrim = { 'e', 'n', 't', 'r', 'i', 's' };
            String strTeachersTabUntrimmedNoofInactiveTeachers = Driver.FindElement(ESKTeachersTabNoofInactiveTeacherElement).Text;
            String strFirstTrimNoofInactiveTeachers = strTeachersTabUntrimmedNoofInactiveTeachers.TrimEnd(charsToTrim);
            String strSecondTrimNoofInactiveTeachers = strFirstTrimNoofInactiveTeachers.Substring(strFirstTrimNoofInactiveTeachers.IndexOf("of") + 2);
            String strNoofInactiveTeachers = strSecondTrimNoofInactiveTeachers.Trim();
            if (strNoofInactiveTeachers.Equals("0"))
            {
                
            }
            else
            {
                IList<IWebElement> TableRows = Driver.FindElements(By.XPath("//tbody[@id='eskTeacherSummaryDataGrid']//tr"));
                for (int i = 1; i <= TableRows.Count; i++)
                {
                    wait.Until(x => x.FindElement(By.XPath("//tbody[@id='eskTeacherSummaryDataGrid']//tr[1]//button[@class='btn blue eskactivate']")).Displayed);
                    Driver.FindElement(By.XPath("//tbody[@id='eskTeacherSummaryDataGrid']//tr[1]//button[@class='btn blue eskactivate']")).Click();
                    Thread.Sleep(2000);
                }
            };
            
            wait.Until(x => x.FindElement(ShowActiveTeacherButton_ESKTeacher));
            Driver.FindElement(ShowActiveTeacherButton_ESKTeacher).Click();
            
        }
        internal void EskTeacherStatus()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            //verify Annual Tab status
            wait.Until(x => x.FindElement(TeacherTabStatusElement));
            string strTeacherTabStatus = Driver.FindElement(TeacherTabStatusElement).Text;
            if (strTeacherTabStatus.Equals("Complete"))
            {
               
            }
            else
            {
                throw new Exception("Fail: Teacher tab status is " + strTeacherTabStatus);
            };
        }

        internal void GoToEsk_Link()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(EarlyStartKindergarten_Element));
            Driver.FindElement(EarlyStartKindergarten_Element).Click();
        }
    }
}
﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Data;
using System.Threading;
using TechTalk.SpecFlow;

namespace KIM_Automation.Pages
{
    class ParentalLeaveApplicationPage : BaseApplicationPage
    {
        public ParentalLeaveApplicationPage(IWebDriver driver) : base(driver)
        { }

        public By SearchTeacher_Element => By.XPath("//div[@id='DataTables_Table_0_filter']//input");

        public By Application_Element => By.XPath("//form[@id='aspnetForm']//tbody[@id='TeacherSummaryDataGrid']/tr[1]/td[5]/span[1]/div[1]/button[1]");

        public By ParentalLeave_Element => By.XPath("//div[@class='dropdown inline open']//button[@class='btn blue pl'][contains(text(),'Parental Leave')]");

        public By MaternityRadioBtn_Element => By.XPath("//div[@class='rbl']//label[contains(text(),'Maternity')]");

        public By DateFrom_Element => By.CssSelector("#txtPLDateFrom");

        public By DateTo_Element => By.CssSelector("#txtPLDateTo");

        public By HourlyRateofPay_Element => By.CssSelector("#txtMAGrossHourlyRate");

        public By WeeklyHours1_Element => By.CssSelector("#txtMATotalWeeklyHours");

        public By WeeklyHours2_Element => By.CssSelector("#txtMATotalWeeklyHoursIncNCT");

        public By NextBtn_Element => By.XPath("//form[@id='parentalLeaveForm']//button[@name='btnNext'][contains(text(),'Next')]");

        public By CertificationName_Element => By.CssSelector("#txtCertNamePL");

        public By Position_Element => By.CssSelector("#txtCertPositionPL");

        public By DateIcon_Element => By.CssSelector("#txtCertDatePL");

        public By DateSelect_Element => By.XPath("//td[@class='today active day']");

        public By SubmitBtn_Element => By.XPath("//form[@id='parentalLeaveForm']//input[@name='btnSubmit']");
        public By TeachersTabElement => By.XPath("//strong[contains(text(),'Teachers')]");
        internal void FillNewParentalLeaveForm(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {

                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));

                wait.Until(x => x.FindElement(MaternityRadioBtn_Element));
                Driver.FindElement(MaternityRadioBtn_Element).Click();

                wait.Until(x => x.FindElement(DateFrom_Element));
                Driver.FindElement(DateFrom_Element).SendKeys(row.ItemArray[0].ToString());

                Thread.Sleep(1000);
                wait.Until(x => x.FindElement(DateTo_Element));
                Driver.FindElement(DateTo_Element).SendKeys(row.ItemArray[1].ToString());

                wait.Until(x => x.FindElement(HourlyRateofPay_Element));
                Driver.FindElement(HourlyRateofPay_Element).SendKeys(row.ItemArray[2].ToString());

                wait.Until(x => x.FindElement(WeeklyHours1_Element));
                Driver.FindElement(WeeklyHours1_Element).SendKeys(row.ItemArray[3].ToString());

                wait.Until(x => x.FindElement(WeeklyHours2_Element));
                Driver.FindElement(WeeklyHours2_Element).SendKeys(row.ItemArray[4].ToString());

                wait.Until(x => x.FindElement(NextBtn_Element));
                Driver.FindElement(NextBtn_Element).Click();

                wait.Until(x => x.FindElement(CertificationName_Element));
                Driver.FindElement(CertificationName_Element).SendKeys(row.ItemArray[5].ToString());

                wait.Until(x => x.FindElement(Position_Element));
                Driver.FindElement(Position_Element).SendKeys(row.ItemArray[6].ToString());

                wait.Until(x => x.FindElement(DateIcon_Element));
                Driver.FindElement(DateIcon_Element).Click();
                //Select the date from date picker

                wait.Until(x => x.FindElement(DateSelect_Element));
                Driver.FindElement(DateSelect_Element).Click();

                wait.Until(x => x.FindElement(SubmitBtn_Element));
                Driver.FindElement(SubmitBtn_Element).Click();
            }

        }
        internal void AddNewParentalLeaveApplication(string teacherName)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.FindElement(SearchTeacher_Element));
            Driver.FindElement(SearchTeacher_Element).Click();
            Driver.FindElement(SearchTeacher_Element).SendKeys(teacherName);
            //Clicking on Application 
            wait.Until(x => x.FindElement(Application_Element));
            Driver.FindElement(Application_Element).Click();
            Thread.Sleep(5000);
            wait.Until(x => x.FindElement(ParentalLeave_Element));
            Driver.FindElement(ParentalLeave_Element).Click();
            Thread.Sleep(3000);
        }

        public void CheckApplicationbutton(string teacherName)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(x => x.FindElement(TeachersTabElement));
            Driver.FindElement(TeachersTabElement).Click();
            wait.Until(x => x.FindElement(SearchTeacher_Element));
            Driver.FindElement(SearchTeacher_Element).Click();
            Driver.FindElement(SearchTeacher_Element).SendKeys(teacherName);

            //Check Application button
            //wait.Until(x => x.FindElement(Application_Element));
            Assert.IsTrue((Driver.FindElement(Application_Element).Enabled), "Application button is not available");

        }
    }
}
﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace KIM_Automation.Pages
{
    class Kim2HomePage : BaseApplicationPage
    {
        public Kim2HomePage(IWebDriver driver) : base(driver)
        { }
        public By ActualFundingStatus_Element => By.XPath("//table[@id='crmFormHeader']//div[@id='header_hsd_fundingstatus']//span[contains(text(),'Ready For Funding')]");
        public IWebElement UserId_Element => Driver.FindElement(By.CssSelector("#userNameInput"));
        public IWebElement Password_Element => Driver.FindElement(By.CssSelector("#passwordInput"));
        public IWebElement SignIn_Element => Driver.FindElement(By.CssSelector("#submitButton"));
        public IWebElement ECService_Element => Driver.FindElement(By.XPath("//a[@id='hsd_ecservice']"));
        public IWebElement Workplace_Element => Driver.FindElement(By.XPath("//span[@id='TabWorkplace']//img[@class='navTabButtonArrowDown']"));
        public IWebElement SearchBox_Element => Driver.FindElement(By.Id("crmGrid_findCriteria"));
        public By ServiceNameLink_Element => By.XPath("//table[@id='gridBodyTable']//a[contains(@id,'gridBodyTable_primaryField_')]");
        public IWebElement Arrow_Element => Driver.FindElement(By.Id("rightNavLink"));
        public By SearchIcon_Element => By.XPath("//table[@id='gridControlBarCompositeControl']//a[@id='crmGrid_findCriteriaButton']");
        public By MarkComplete_Task => By.XPath("//li[@id='task|NoRelationship|Form|Mscrm.Form.task.SaveAsComplete']");
        public By SigOut_Name_Link => By.XPath("//a[@id='navTabButtonUserInfoLinkId']");
        public By SigOut_Link => By.XPath("//a[@id='navTabButtonUserInfoSignOutId']");
        public IWebElement Service_Overlay => Driver.FindElement(By.XPath("//span[@id='TabNode_tab0Tab']//a[@class='navTabButtonLink']"));
        public IWebElement Child_Overlay => Driver.FindElement(By.XPath("//span[@id='TabNode_tab0Tab']//a[@class='navTabButtonLink']"));
        public IWebElement Sercive_Activities => Driver.FindElement(By.XPath("//a[@id='Node_navActivities']"));

        public IWebElement PreCommit_Overlay => Driver.FindElement(By.XPath("//span[@id='TabNode_tab0Tab']//img[@class='navTabButtonArrowDown']"));

        internal void SignIn_COEditor()
        {
            string userId = CommonFunctions.GetResourceValue("Kim2UserId_COEditor");
            CommonFunctions.EnterText(UserId_Element, userId);
            string password = CommonFunctions.GetResourceValue("Kim2Password_COEditor");
            CommonFunctions.EnterText(Password_Element, password);
            CommonFunctions.GetClick(SignIn_Element);
        }
        internal void SignIn_COApprover()
        {
            string userId = CommonFunctions.GetResourceValue("Kim2UserId_COApprover");
            CommonFunctions.EnterText(UserId_Element, userId);
            string password = CommonFunctions.GetResourceValue("Kim2Password_COApprover");
            CommonFunctions.EnterText(Password_Element, password);
            CommonFunctions.GetClick(SignIn_Element);
        }
        internal void SignIn_ROEditor()
        {
            string userId = CommonFunctions.GetResourceValue("Kim2UserId_ROEditor");
            CommonFunctions.EnterText(UserId_Element, userId);
            string password = CommonFunctions.GetResourceValue("Kim2Password_ROEditor");
            CommonFunctions.EnterText(Password_Element, password);
            CommonFunctions.GetClick(SignIn_Element);
        }
        internal void SignIn_ROApprover()
        {
            string userId = CommonFunctions.GetResourceValue("Kim2UserId_ROApprover");
            CommonFunctions.EnterText(UserId_Element, userId);
            string password = CommonFunctions.GetResourceValue("Kim2Password_ROApprover");
            CommonFunctions.EnterText(Password_Element, password);
            CommonFunctions.GetClick(SignIn_Element);
        }
        internal void SignIn_COSystemAdmin()
        {
            string userId = CommonFunctions.GetResourceValue("Kim2UserId_COSystemAdmin");
            CommonFunctions.EnterText(UserId_Element, userId);
            string password = CommonFunctions.GetResourceValue("Kim2Password_COSystemAdmin");
            CommonFunctions.EnterText(Password_Element, password);
            CommonFunctions.GetClick(SignIn_Element);
        }

        internal void SearchForAddedService(IWebDriver Driver, string addedNewService)

        {     
            Thread.Sleep(5000);
            Workplace_Element.Click();
            Thread.Sleep(2000);
            //Display the overlay
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].style='display: block;'", Driver.FindElement(By.Id("navBarOverlay")));
            Arrow_Element.Click(); //If logged in as system admin
            Thread.Sleep(2000);
            ECService_Element.Click();
            //Hide the overlay 
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].style='display: none;'", Driver.FindElement(By.Id("navBarOverlay")));
            // Switch to the frame where element exists
            Driver.SwitchTo().Frame("contentIFrame0");
            SearchBox_Element.Click();
            SearchBox_Element.SendKeys(addedNewService);
            Thread.Sleep(3000);
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            waitAgency.Until(c => c.FindElement(SearchIcon_Element));
            Driver.FindElement(SearchIcon_Element).Click();
            waitAgency.Until(c => c.FindElement(ServiceNameLink_Element));
            Driver.FindElement(ServiceNameLink_Element).Click();
            Driver.SwitchTo().DefaultContent();
        }

        internal void SearchForAddedService_Kimeditorapprover(IWebDriver Driver, string addedNewService)

        {
            Thread.Sleep(5000);
            Workplace_Element.Click();
            Thread.Sleep(3000);
            //Display the overlay
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].style='display: block;'", Driver.FindElement(By.Id("navBarOverlay")));
            //Arrow_Element.Click(); //If kim editor is logged in
            Thread.Sleep(3000);
            ECService_Element.Click();
            //Hide the overlay 
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].style='display: none;'", Driver.FindElement(By.Id("navBarOverlay")));
            // Switch to the frame where element exists
            Driver.SwitchTo().Frame("contentIFrame0");
            Thread.Sleep(3000);
            SearchBox_Element.Click();
            SearchBox_Element.SendKeys(addedNewService);
            Thread.Sleep(7000);
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            waitAgency.Until(c => c.FindElement(SearchIcon_Element));
            Thread.Sleep(3000);
            Driver.FindElement(SearchIcon_Element).Click();
            waitAgency.Until(c => c.FindElement(ServiceNameLink_Element));
            Thread.Sleep(2000);
            Driver.FindElement(ServiceNameLink_Element).Click();
            Driver.SwitchTo().DefaultContent();
        }

        internal void ServiceNameLink_Click()
        {
            Thread.Sleep(10000);
            Driver.SwitchTo().Frame("contentIFrame0"); 
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            waitAgency.Until(c => c.FindElement(ServiceNameLink_Element));
            Driver.FindElement(ServiceNameLink_Element).Click();
            Driver.SwitchTo().DefaultContent();
      
        }
        
        internal void Verify_ServiceStatus()
        {
            Driver.SwitchTo().Frame("contentIFrame1");
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            waitAgency.Until(c => c.FindElement(ActualFundingStatus_Element));
            Thread.Sleep(10000);
            Assert.IsTrue(Driver.FindElement(ActualFundingStatus_Element).Displayed);
            Driver.SwitchTo().DefaultContent();
        }
        internal void SignOut()
        {
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            waitAgency.Until(c => c.FindElement(SigOut_Name_Link));
            Driver.FindElement(SigOut_Name_Link).Click();
            waitAgency.Until(c => c.FindElement(SigOut_Link));
            Driver.FindElement(SigOut_Link).Click();
        }
        
        internal void NavigateToTask()
        {
            Thread.Sleep(12000);
            Service_Overlay.Click();
            Thread.Sleep(2000);
            //Display the overlay
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].style='display: block;'", Driver.FindElement(By.Id("navBarOverlay")));
            Thread.Sleep(7000);
            Sercive_Activities.Click();
            //Hide the overlay 
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].style='display: none;'", Driver.FindElement(By.Id("navBarOverlay")));
        }

        internal void NavigateToTask_PreCommitment()
        {
            Thread.Sleep(12000);
            PreCommit_Overlay.Click();
            Thread.Sleep(5000);
            //Display the overlay
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].style='display: block;'", Driver.FindElement(By.Id("navBarOverlay")));
            Thread.Sleep(2000);
            Sercive_Activities.Click();
            //Hide the overlay 
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].style='display: none;'", Driver.FindElement(By.Id("navBarOverlay")));
        }


        internal void NavigateToTask_Child(){
            Thread.Sleep(12000);
            Child_Overlay.Click();
            Thread.Sleep(5000);
            //Display the overlay
            ((IJavaScriptExecutor) Driver).ExecuteScript("arguments[0].style='display: block;'", Driver.FindElement(By.Id("navBarOverlay")));
            Thread.Sleep(5000);
            Sercive_Activities.Click();
            //Hide the overlay 
            ((IJavaScriptExecutor) Driver).ExecuteScript("arguments[0].style='display: none;'", Driver.FindElement(By.Id("navBarOverlay")));
        }

    internal void TaskComplete(string task)
        {
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            //Switch to the frames where element can be located
            Driver.SwitchTo().Frame("contentIFrame1");
            Driver.SwitchTo().Frame("areaActivitiesFrame");
            Thread.Sleep(12000);
            Driver.FindElement(By.PartialLinkText(task)).Click();
            //Switch back to default and come out of iframes
            Driver.SwitchTo().ParentFrame();  
            Driver.SwitchTo().DefaultContent();
            //Mark the task complete
            Thread.Sleep(5000);
            waitAgency.Until(c => c.FindElement(MarkComplete_Task));
            Driver.FindElement(MarkComplete_Task).Click();
            Thread.Sleep(5000);
        }

        internal void VerifyTask(string task)
        {
         
            //Switch to the frames where element can be located
            Driver.SwitchTo().Frame("contentIFrame1");
            Driver.SwitchTo().Frame("areaActivitiesFrame");
            Thread.Sleep(10000);
            //Verify the task exists
            Assert.IsTrue(Driver.FindElement(By.PartialLinkText(task)).Displayed,"The task does not exist");
            //Switch back to default and come out of iframes
            Driver.SwitchTo().ParentFrame();
            Driver.SwitchTo().DefaultContent();
        }
        
    }
}
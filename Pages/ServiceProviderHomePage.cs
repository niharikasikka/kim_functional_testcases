﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Data;
using System.Threading;
using TechTalk.SpecFlow;
using System.Windows.Forms;
using System.Linq;
using System;
using NUnit.Framework;
using System.Collections.Generic;

namespace KIM_Automation.Pages
{
    class ServiceProviderHomePage : BaseApplicationPage
    {
        public ServiceProviderHomePage(IWebDriver driver) : base(driver)
        { }
        public By AddServiceButton_Element => By.XPath("//button[contains(@id,'ServiceAddEditButton')]");
        public IWebElement ServiceName_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_ServiceName']"));
        public IWebElement DateCommence_Element => Driver.FindElement(By.XPath("//button[@id='ctl00_ctl50_g_7cd0e0c1_b447_4440_bd0f_51aadf0767f5_ctl00_ServiceAddEditButton']"));
        public IWebElement UnitNum_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_LocAddressNumber']"));
        public IWebElement StreetNum_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_LocAddressStreetName']"));
        public IWebElement StreetType_Element => Driver.FindElement(By.XPath("//select[@id='ctl01_addSrv_LocAddressStreetType']"));
        public IWebElement Suburb_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_LocAddressSuburb']"));
        public IWebElement State_Element => Driver.FindElement(By.XPath("//select[@id='ctl01_addSrv_LocAddressState']"));
        public IWebElement Lga_Element => Driver.FindElement(By.XPath("//select[@id='ctl02_addSrv_VMASLGACodes']"));
        public IWebElement Postcode_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_LocAddressPostCode']"));
        public By LgaCode_Element => By.CssSelector("#btnSelectLGA");
        public By Tele_Element => By.XPath("//input[@id='addSrv_Telephone']");
        public IWebElement Email_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_Email']"));
        public IWebElement TitleEmergency_Element => Driver.FindElement(By.XPath("//select[@id='ctl01_addSrv_EmergencyContactTitle']"));
        public IWebElement GivenNameEmergency_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_EmergencyContactFirstName']"));
        public IWebElement FamilyNameEmergency_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_EmergencyContactSurName']"));
        public By New_ServiceProvider => By.XPath("//input[@id='trnSrvOut_receivingServiceProvider']");
        public By Information_Transfer => By.XPath("//input[@id='trnSrvOut_shareInfoYes']");
        public By TransferForm_Submit => By.XPath("//div[@id='serviceTransferOutForm']//input[@name='btnSubmit']");
        //public By New_OrgId => By.XPath("//input[@id='trnSrvOut_receivingOrgID']");
        public By Apply_EymOrg_Button => By.XPath("//button[@class='btn blue apply-cluster']");
        public By EymApproval_Council_No => By.XPath("//input[@id='clusterSrv_rbOptCounsilClusterApproval2']");
        public By EymForm_SubmitButton => By.XPath("//div[@id='clusterApplyFormFooter']//input[@name='btnSubmit']");
        public IWebElement EmailEmergency_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_EmergencyContactEmail']"));
        public IWebElement PositionEmergency_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_EmergencyContactPosition']"));
        public IWebElement TeleEmergency_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_EmergencyContactTelephone']"));
        public IWebElement MobileEmergency_Element => Driver.FindElement(By.XPath("//input[@id='addSrv_EmergencyContactMobile']"));
        public IWebElement SeviceOperatorCouncil_Element => Driver.FindElement(By.XPath("//input[@id='rbaddSrvIsOutOfCouncilFacility1']"));
        public IWebElement QualityRating_Element => Driver.FindElement(By.CssSelector("#ctl01_addSrv_NationalQualityFrameWorkRating"));
        public IWebElement ProgramOffered_Element => Driver.FindElement(By.CssSelector("#rbaddSrvIsFundedKindergarten1"));
        public IWebElement NextButton_Element => Driver.FindElement(By.XPath("//button[@name='btnNext']"));
        public IWebElement InsuranceCover_Element => Driver.FindElement(By.CssSelector("#rbaddSrvIsAssociationInc2"));
        public IWebElement Eym_Element => Driver.FindElement(By.CssSelector("#rbaddSrvApplyForCluster2"));
        public IWebElement EymYes_Element => Driver.FindElement(By.XPath("//input[@id='rbaddSrvApplyForCluster1']"));
        public IWebElement EducationNature_Element => Driver.FindElement(By.CssSelector("#rbaddSrvNatureOfEducationCare1"));
        public IWebElement SubmitButton_Element => Driver.FindElement(By.XPath("//div[@id='serviceAddEditForm']//input[@name='btnSubmit']"));
        public IWebElement UploadButton_Element => Driver.FindElement(By.CssSelector("#cmUpload"));
        public IWebElement ChooseFile_Element => Driver.FindElement(By.CssSelector("#fileUploadQualifications"));
        public IWebElement CalendarIcon_Element => Driver.FindElement(By.XPath("//div[@class='fullWidth']//span[@class='input-group-addon']"));
        public IWebElement CalendarDate_Element => Driver.FindElement(By.XPath("//td[@class='today active day']"));
        public By LgaRadio_Element => By.CssSelector("#rbaddSrvVMASOption2");
        public IWebElement LgaSubmitButton_Element => Driver.FindElement(By.XPath("//div[@id='serviceVMASForm']//input[@name='btnSelect']"));
        public IWebElement UploadButtonFile_Element => Driver.FindElement(By.CssSelector("#btnUploadFile"));
        public IWebElement SaveButtonFile_Element => Driver.FindElement(By.CssSelector("#btnSaveQuals"));
        public IWebElement LeagalCouncil_Element => Driver.FindElement(By.CssSelector("#clusterSrv_rbOptCounsilClusterApproval2"));
        public IWebElement SubmitButton2_Element => Driver.FindElement(By.XPath("//div[@id='clusterApplyFormFooter']//input[@name='btnSubmit']"));
        string NewService;
        //public By SearchBox_Element => By.XPath("//label[contains(text(),'earch')]");
        public By SearchBox_Element => By.XPath("//label[contains(text(),'Search:')]//input");
        //public By SearchBox_Element => By.XPath("//*[@id='serviceFundingSummaryTable_filter']/label/input");
        //public By SearchBox_Element => By.XPath("/html[1]/body[1]/form[1]/div[5]/div[1]/div[1]/div[1]/div[6]/div[2]/span[5]/table[1]/tbody[1]/tr[3]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/label[1]/input[1]");
        public By NewServiceExists_Element => By.XPath("//td[contains(@class,'sorting_1')]");
        //public By NewServiceExists_Element => By.CssSelector("body.ms-backgroundImage:nth-child(2) div.container div.ms-webpart-zone.ms-fullWidth div.s4-wpcell-plain.ms-webpartzone-cell.ms-webpart-cell-vertical.ms-fullWidth div.ms-webpart-chrome.ms-webpart-chrome-vertical.ms-webpart-chrome-fullWidth div.ms-WPBody div.dataTables_wrapper:nth-child(5) table.customTable.serv-list.dataTable tbody:nth-child(2) tr.odd td.sorting_1:nth-child(1) > a:nth-child(1)");
        WebDriverWait waitAgency;
        public IWebElement CloseButton => Driver.FindElement(By.Name("btnClose"));
        public IWebElement AnnualConfTab_Element => Driver.FindElement(By.XPath("//div[@class='tabs datacollectiontabs']//li[5]"));
        public By EditButton_Element => By.XPath("//tr[contains(@class,'odd')]//button[contains(@class,'btn blue edit-apply')]");
        public By RemoveButton_Element => By.CssSelector("#rbDeleteService");
        public By EditButton_Remove => By.XPath("//button[@name='btnEdit']");
        public By RadioButton_Element => By.XPath("//input[@id='remSrv_rbServiceClosing']");
        public By SubmitButton_Remove => By.XPath("//input[@id='btnSubmit']");
        public By ReasonTextBox_Element => By.CssSelector("#remSrv_ceaseReason");
        public By CeaseDate_Element => By.XPath("//input[@id='remSrv_ceasedDate']");
        public By TransferDate_Element => By.XPath("//input[@id='trnSrvOut_serviceTransferDate']");
        public IWebElement SelectDate_Today => Driver.FindElement(By.XPath("//td[@class='today active day']"));
        public IWebElement SelectDate_Past => Driver.FindElement(By.XPath("//td[@class='old day'][contains(text(),'30')]"));
        public By EditServiceButton_Element => By.CssSelector("#rbEditService");
        public By ReportsTab_Element => By.CssSelector("#ctl00_kimheader_ybsReports");
        public By ServiceProviderReport_Element => By.CssSelector("#ctl00_kimheader_spReportLink");
        public By ServiceReport_Element => By.CssSelector("#ctl00_kimheader_spServiceReports");
        public By EymFundingStatus_Submitted => By.XPath("//tbody[@id='servicesSummaryDataGrid']//tr[1]//a[contains(text(),'Submitted')]");
        public By TransferButton_Element => By.XPath("//div[@id='serviceEditApplyModal']//input[@id='rbTransferService']");
        public By ApplyButton => By.XPath("//div[@id='serviceEditApplyModal']//button[@name='btnApply']");
        public By ApplyEym_Radio => By.XPath("//div[@id='serviceEditApplyModal']//input[@id='rbClusterFunding']");
        public By Recommence_Radio => By.CssSelector("#rbRecommence");
        public By EymFundingStatus_Approved => By.LinkText("Approved");
        public By ServiceForm_element => By.XPath("//div[@id='serviceAddEditForm']");
        public By ServiceDateCommenced_element => By.CssSelector("#addSrv_DateCommenced");
        public By ServiceDateCommenced_day_element => By.XPath("//td[@class='today active day']");
        public By SvcNumber_element => By.CssSelector("#addSrv_LocAddressNumber");
        public By SvcStreet_element => By.CssSelector("#addSrv_LocAddressStreetName");
        public By SvcStreetType_element => By.CssSelector("#ctl01_addSrv_LocAddressStreetType");
        public By SvcSuburb_element => By.CssSelector("#addSrv_LocAddressSuburb");
        public By SvcState_element => By.CssSelector("#ctl01_addSrv_LocAddressState");
        public By SvcPostCode_element => By.CssSelector("#addSrv_LocAddressPostCode");
        public By SvcLGACode_element => By.CssSelector("#addSrv_LGACode");
        public By SvcLGACode_radio_element => By.CssSelector("#addSrv_LGACode");
        public By SvcTelephone_element => By.CssSelector("#addSrv_Telephone");
        public By SvcEmail_element => By.CssSelector("#addSrv_Email");        
        public By SvcECDTitle_element => By.CssSelector("#ctl01_addSrv_EmergencyContactTitle");
        public By SvcECDGivenName_element => By.CssSelector("#addSrv_EmergencyContactFirstName");
        public By SvcECDFamilyName_element => By.CssSelector("#addSrv_EmergencyContactSurName");
        public By SvcECDEmail_element => By.CssSelector("#addSrv_EmergencyContactEmail");
        public By SvcECDPosition_element => By.CssSelector("#addSrv_EmergencyContactPosition");
        public By SvcECDTelephone_element => By.CssSelector("#addSrv_EmergencyContactTelephone");
        public By SvcECDMobile_element => By.CssSelector("#addSrv_EmergencyContactMobile");        
        public By SvcOutofCouncilYes_radio_element => By.CssSelector("#rbaddSrvIsOutOfCouncilFacility1");
        public By SvcTicktoContinue_checkbox_element => By.CssSelector("#addSrvchkIgnoreErrors");
        public By SvcIsAssociation_radio_element => By.CssSelector("#rbaddSrvIsAssociationInc1");
        public By SvcLegalEntity_element => By.CssSelector("#addSrv_LegalEntity");
        public By SvcEYM_Yes_radio_element => By.CssSelector("#rbaddSrvApplyForCluster1");
        public By SvcEYM_No_radio_element => By.CssSelector("#rbaddSrvApplyForCluster2");
        public By SvcNatureofEducCare_radio_element => By.CssSelector("#rbaddSrvNatureOfEducationCare1");
        public By SvcNationalQualityFrameworkRating_dropdown_element => By.CssSelector("#ctl01_addSrv_NationalQualityFrameWorkRating");
        public By SvcFundedKindergarten_radio_element => By.CssSelector("#rbaddSrvIsFundedKindergarten1");

        public By EymForm_element => By.XPath("//div[@id='serviceClusterApplyModal']");
        public By EymFormEymOrgApproval_No_element => By.CssSelector("#clusterSrv_rbOptCounsilClusterApproval2");
        public By EymForm_ExitButton => By.XPath("//div[@id='clusterApplyFormFooter']//button[@name='btnExit']");
        String winHandleBefore;
        public By Back_Button => By.XPath("//div[@class='datepicker-days']//th[@class='prev'][contains(text(),'«')]");
        internal string FillForm_NoEym(Table table)
        {
            try
            {
                var dataTable = TableExtensions.ToDataTable(table);
                string numRows = dataTable.Rows.ToString();
                foreach (DataRow row in dataTable.Rows)
                {

                    //Storing the name of the new service added
                    /*//random generator of Service Name
                    Random generatorSn = new Random();
                    int iSn = generatorSn.Next(1, 1000000);
                    string sSn = iSn.ToString().PadLeft(6, '0');
                    string service = "SVC-" + sSn;
                    NewService = service;*/

                    NewService = row.ItemArray[0].ToString();
                    

                    //Passing all the elements from the feature file to fill in the form
                    ServiceName_Element.SendKeys(NewService);
                    Thread.Sleep(5000);
                    //Selecting a future date
                    CalendarIcon_Element.Click();
                    CalendarDate_Element.Click();

                    //random generator of Unit number
                    Random generatorUn = new Random();
                    int iUn = generatorUn.Next(1, 1000000);
                    string unitNum = iUn.ToString().PadLeft(6, '0');
                    //removed UnitNum_Element.SendKeys(row.ItemArray[2].ToString());
                    UnitNum_Element.SendKeys(unitNum);
                    Thread.Sleep(5000);

                    //random generator of Street
                    Random generatorStn = new Random();
                    int iStn = generatorStn.Next(1, 1000000);
                    string sStn = iStn.ToString().PadLeft(6, '0');
                    string stnum = "Str-" + DateTime.Now.ToString("ddmmyyyy") + "_" + sStn;

                    //removed StreetNum_Element.SendKeys(row.ItemArray[3].ToString());
                    StreetNum_Element.SendKeys(stnum);

                    //Selecting a street type from dropdown
                    var selectElement_street = new SelectElement(StreetType_Element);
                    selectElement_street.SelectByText(row.ItemArray[4].ToString());

                    Suburb_Element.SendKeys(row.ItemArray[5].ToString());
                    //Selecting a state from dropdown
                    var selectElement_state = new SelectElement(State_Element);
                    selectElement_state.SelectByText(row.ItemArray[6].ToString());

                    //random generator of Post Code
                    Random generatorPc = new Random();
                    int iPc = generatorPc.Next(1111, 9999);
                    string postCode = iPc.ToString().PadLeft(3, '0');
                    // removed Postcode_Element.SendKeys(row.ItemArray[7].ToString());
                    Postcode_Element.SendKeys(postCode);

                    State_Element.Click();

                    //Selecting LGA Code
                    waitAgency.Until(c => c.FindElement(SvcLGACode_element));
                    Driver.FindElement(SvcLGACode_element).Click();
                    waitAgency.Until(c => c.FindElement(LgaCode_Element));
                    Driver.FindElement(LgaCode_Element).Click();

                    Thread.Sleep(3000);
                    waitAgency.Until(c => c.FindElement(LgaRadio_Element));
                    Driver.FindElement(LgaRadio_Element).Click();
                    var selectElement_lga = new SelectElement(Lga_Element);
                    selectElement_lga.SelectByText(row.ItemArray[8].ToString());
                    LgaSubmitButton_Element.Click();
                    Thread.Sleep(3000);
                    Driver.FindElement(Tele_Element).SendKeys(row.ItemArray[9].ToString());
                    Thread.Sleep(2000);
                    Email_Element.SendKeys(row.ItemArray[10].ToString());
                    //Selecting emergency title from dropdown
                    var selectElement_title = new SelectElement(TitleEmergency_Element);
                    selectElement_title.SelectByText(row.ItemArray[11].ToString());
                    //Filling emergecy details
                    GivenNameEmergency_Element.SendKeys(row.ItemArray[12].ToString());
                    FamilyNameEmergency_Element.SendKeys(row.ItemArray[13].ToString());
                    Thread.Sleep(3000);
                    EmailEmergency_Element.SendKeys(row.ItemArray[14].ToString());
                    Thread.Sleep(2000);
                    PositionEmergency_Element.SendKeys(row.ItemArray[15].ToString());
                    Thread.Sleep(2000);
                    TeleEmergency_Element.SendKeys(row.ItemArray[16].ToString());
                    Thread.Sleep(2000);
                    MobileEmergency_Element.SendKeys(row.ItemArray[17].ToString());

                    //Selecting council facility radio button
                    SeviceOperatorCouncil_Element.Click();
                    //Forced wait for 3 seconds
                    Thread.Sleep(3000);
                    //Filling in details on next page
                    NextButton_Element.Click();
                    InsuranceCover_Element.Click();
                    Eym_Element.Click();
                    EducationNature_Element.Click();
                    Thread.Sleep(2000);
                    //Selecting quality rating
                    var selectElement_rating = new SelectElement(QualityRating_Element);
                    selectElement_rating.SelectByText(row.ItemArray[18].ToString());
                    ProgramOffered_Element.Click();
                    Thread.Sleep(5000);
                    SubmitButton_Element.Click();
                    Thread.Sleep(5000);
                                       
                }
            }
            catch (System.EntryPointNotFoundException ex)
            {
                System.Console.WriteLine(ex.ToString());

            }
            return NewService;
        }

        internal void Verify_ServiceTransfer(string service)
        {
            
        }

        internal void FillForm_TransferService(string serviceProvider)
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(TransferDate_Element));
            Thread.Sleep(2000);
            Driver.FindElement(TransferDate_Element).Click();
            Thread.Sleep(1000);
            SelectDate_Today.Click();
            waitAgency.Until(c => c.FindElement(New_ServiceProvider));
            Driver.FindElement(New_ServiceProvider).Click();
            Driver.FindElement(New_ServiceProvider).SendKeys(serviceProvider);
            //Select the hidden service provider name 
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].style='display: block;'", Driver.FindElement(By.Id("ui-id-2")));
            Driver.FindElement(By.LinkText(serviceProvider)).Click();
            //Hide the overlay 
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].style='display: none;'", Driver.FindElement(By.Id("ui-id-2")));

            waitAgency.Until(c => c.FindElement(Information_Transfer));
            Driver.FindElement(Information_Transfer).Click();
            //submit the form
            waitAgency.Until(c => c.FindElement(TransferForm_Submit));
            Driver.FindElement(TransferForm_Submit).Click();
        }

        internal void EditService_Transfer(string service)
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(EditButton_Element));
            Driver.FindElement(EditButton_Element).Click();
            //Wait for dialog to open
            Thread.Sleep(2000);
            waitAgency.Until(c => c.FindElement(TransferButton_Element));
            Driver.FindElement(TransferButton_Element).Click();
            waitAgency.Until(c => c.FindElement(EditButton_Remove));
            Driver.FindElement(EditButton_Remove).Click();
        }

        internal void Verify_EymFunding_Submitted(string service)
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(SearchBox_Element));
            //Search for service
            Driver.FindElement(SearchBox_Element).Click();
            Driver.FindElement(SearchBox_Element).SendKeys(service);
            waitAgency.Until(c => c.FindElement(EymFundingStatus_Submitted));
            Assert.IsTrue(Driver.FindElement(EymFundingStatus_Submitted).Displayed, "The service is not submitted for EYM funding");
        }

        
        internal void Apply_EymOrg()
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(Apply_EymOrg_Button));
            Driver.FindElement(Apply_EymOrg_Button).Click();
            //Filling up details for EYM service
            var winHandleBefore = Driver.CurrentWindowHandle;
            Thread.Sleep(2000);
            UploadButton_Element.Click();
            Thread.Sleep(2000);
            //Switch to upload window
            Driver.SwitchTo().Window(Driver.WindowHandles.Last());
            //Uploading the file using keyboard actions
            Thread.Sleep(2000);
            ChooseFile_Element.Click();
            Thread.Sleep(2000);
            var fileNamePath = CommonFunctions.GetResourceValue("FileUpload_EymService");
            SendKeys.SendWait(fileNamePath);
            SendKeys.SendWait(@"{Enter}");
            Thread.Sleep(2000);
            UploadButtonFile_Element.Click();
            SaveButtonFile_Element.Click();
            //Brings the control back to original window
            Driver.SwitchTo().Window(winHandleBefore);
            //Local council Eym Orrg select no
            waitAgency.Until(c => c.FindElement(EymApproval_Council_No));
            Driver.FindElement(EymApproval_Council_No).Click();
            waitAgency.Until(c => c.FindElement(EymForm_SubmitButton));
            Driver.FindElement(EymForm_SubmitButton).Click();
            //Navigate to service list page
            Driver.Navigate().Back();
        }

        internal void EditService_EymFunding(string service)
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(EditButton_Element));
            Driver.FindElement(EditButton_Element).Click();
            //Wait for dialog to open
            Thread.Sleep(2000);
            waitAgency.Until(c => c.FindElement(ApplyEym_Radio));
            Driver.FindElement(ApplyEym_Radio).Click();
            waitAgency.Until(c => c.FindElement(ApplyButton));
            Driver.FindElement(ApplyButton).Click();
        }

        internal string FillForm_NoEym_MultipleServices(Table table)
        {
            try
            {
                var dataTable = TableExtensions.ToDataTable(table);
                string numRows = dataTable.Rows.ToString();
                foreach (DataRow row in dataTable.Rows)
                {
                    //Storing the name of the new service added
                    //Storing the name of the new service added
                    /*//random generator of Service Name
                    Random generatorSn = new Random();
                    int iSn = generatorSn.Next(1, 1000000);
                    string sSn = iSn.ToString().PadLeft(6, '0');
                    string service = "SVC-" + sSn;
                    NewService = service;*/

                    NewService = row.ItemArray[0].ToString();
                    
                    //Passing all the elements from the feature file to fill in the form
                    ServiceName_Element.SendKeys(NewService);

                    Thread.Sleep(5000);
                    //Selecting a future date
                    CalendarIcon_Element.Click();
                    CalendarDate_Element.Click();
                    UnitNum_Element.SendKeys(row.ItemArray[2].ToString());
                    Thread.Sleep(5000);
                    StreetNum_Element.SendKeys(row.ItemArray[3].ToString());
                    //Selecting a street type from dropdown
                    var selectElement_street = new SelectElement(StreetType_Element);
                    selectElement_street.SelectByText(row.ItemArray[4].ToString());

                    Suburb_Element.SendKeys(row.ItemArray[5].ToString());
                    //Selecting a state from dropdown
                    var selectElement_state = new SelectElement(State_Element);
                    selectElement_state.SelectByText(row.ItemArray[6].ToString());

                    Postcode_Element.SendKeys(row.ItemArray[7].ToString());
                    State_Element.Click();
                    Thread.Sleep(5000);
                    //Selecting LGA Code
                    waitAgency.Until(c => c.FindElement(SvcLGACode_element));
                    Driver.FindElement(SvcLGACode_element).Click();
                    waitAgency.Until(c => c.FindElement(LgaCode_Element));
                    Driver.FindElement(LgaCode_Element).Click();

                    waitAgency.Until(c => c.FindElement(LgaRadio_Element));
                    Driver.FindElement(LgaRadio_Element).Click();
                    var selectElement_lga = new SelectElement(Lga_Element);
                    selectElement_lga.SelectByText(row.ItemArray[8].ToString());
                    LgaSubmitButton_Element.Click();
                    Thread.Sleep(3000);
                    Driver.FindElement(Tele_Element).SendKeys(row.ItemArray[9].ToString());
                    Thread.Sleep(2000);
                    Email_Element.SendKeys(row.ItemArray[10].ToString());
                    //Selecting emergency title from dropdown
                    var selectElement_title = new SelectElement(TitleEmergency_Element);
                    selectElement_title.SelectByText(row.ItemArray[11].ToString());
                    //Filling emergecy details
                    GivenNameEmergency_Element.SendKeys(row.ItemArray[12].ToString());
                    FamilyNameEmergency_Element.SendKeys(row.ItemArray[13].ToString());
                    Thread.Sleep(3000);
                    EmailEmergency_Element.SendKeys(row.ItemArray[14].ToString());
                    Thread.Sleep(2000);
                    PositionEmergency_Element.SendKeys(row.ItemArray[15].ToString());
                    Thread.Sleep(2000);
                    TeleEmergency_Element.SendKeys(row.ItemArray[16].ToString());
                    Thread.Sleep(2000);
                    MobileEmergency_Element.SendKeys(row.ItemArray[17].ToString());

                    //Selecting council facility radio button
                    SeviceOperatorCouncil_Element.Click();
                    //Forced wait for 3 seconds
                    Thread.Sleep(3000);
                    //Filling in details on next page
                    NextButton_Element.Click();
                    InsuranceCover_Element.Click();
                    Eym_Element.Click();
                    EducationNature_Element.Click();
                    Thread.Sleep(2000);
                    //Selecting quality rating
                    var selectElement_rating = new SelectElement(QualityRating_Element);
                    selectElement_rating.SelectByText(row.ItemArray[18].ToString());
                    ProgramOffered_Element.Click();
                    Thread.Sleep(5000);
                    SubmitButton_Element.Click();
                    Thread.Sleep(2000);
                    AddService();

                }
            }
            catch (System.EntryPointNotFoundException ex)
            {
                System.Console.WriteLine(ex.ToString());

            }
            return NewService;
        }
  
        internal void AddedSuccess()
        {
            //Thread.Sleep(5000);
            Assert.IsTrue(Driver.FindElement(AddServiceButton_Element).Displayed,"Service not added successfully");
        }

        
        internal void AnnualConfirmation()
        {
            Thread.Sleep(5000);
            Assert.IsTrue(AnnualConfTab_Element.Enabled, "Annual confirmation tab is not enabled");
        }

        internal void SearchService(string NewService)
        {

            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(SearchBox_Element));
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            Driver.FindElement(SearchBox_Element).Click();
            Driver.FindElement(SearchBox_Element).SendKeys(NewService);
            waitAgency.Until(c => c.FindElement(NewServiceExists_Element));
            Driver.FindElement(By.PartialLinkText(NewService)).Click();
            
        }
        internal void Reports()
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(100));
            waitAgency.Until(c => c.FindElement(ReportsTab_Element));
            Driver.FindElement(ReportsTab_Element).Click();
        }

        internal void ServiceProviderReports()
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(100));
            waitAgency.Until(c => c.FindElement(ServiceProviderReport_Element));
            Driver.FindElement(ServiceProviderReport_Element).Click();
        }
        internal void ServiceReports()
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(100));
            waitAgency.Until(c => c.FindElement(ServiceReport_Element));
            Driver.FindElement(ServiceReport_Element).Click();
        }

        internal void EditPhone(string telephone)
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(Tele_Element));
            Driver.FindElement(Tele_Element).Clear();
            Driver.FindElement(Tele_Element).SendKeys(telephone);
            SubmitButton_Element.Click();
        }

        internal string EditService(string service)
        {
            winHandleBefore = Driver.CurrentWindowHandle;
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(EditButton_Element));
            Driver.FindElement(EditButton_Element).Click();
            Thread.Sleep(2000);
            waitAgency.Until(c => c.FindElement(EditServiceButton_Element));
            Driver.FindElement(EditServiceButton_Element).Click();
            //Click on edit button to fill more details
            waitAgency.Until(c => c.FindElement(EditButton_Remove));
            Driver.FindElement(EditButton_Remove).Click();
            return winHandleBefore;
        }

        internal void Verify_EditService(string service, string winHandleBefore)
        {

            Driver.SwitchTo().Window(winHandleBefore);

            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(EditButton_Element));
            Driver.FindElement(EditButton_Element).Click();
            Thread.Sleep(2000); 
            Assert.AreEqual(Driver.FindElement(EditServiceButton_Element).Enabled.ToString(), "False");
            
        }

        internal void FindService_EditDelete(string NewService)
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(SearchBox_Element));
           
            Driver.FindElement(SearchBox_Element).Click();
            //Driver.FindElement(SearchBox_Element).Clear();
            Driver.FindElement(SearchBox_Element).SendKeys(NewService);
        }

        internal void AddService()
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(AddServiceButton_Element));
            Driver.FindElement(AddServiceButton_Element).Click();
            Thread.Sleep(3000);
        }

      
        public String FillForm_Eym(Table table) 
        {
            try
            {
                var dataTable = TableExtensions.ToDataTable(table);
                foreach (DataRow row in dataTable.Rows)
                {
                    //Storing the name of the new service added
                    //Storing the name of the new service added
                    /*//random generator of Service Name
                    Random generatorSn = new Random();
                    int iSn = generatorSn.Next(1, 1000000);
                    string sSn = iSn.ToString().PadLeft(6, '0');
                    string service = "SVC-" + sSn;
                    NewService = service;*/

                    NewService = row.ItemArray[0].ToString();
                    

                    //Passing all the elements from the feature file to fill in the form
                    ServiceName_Element.SendKeys(NewService);

                    //Thread.Sleep(5000);
                    //Selecting a future date
                    CalendarIcon_Element.Click();
                    CalendarDate_Element.Click();
                    UnitNum_Element.SendKeys(row.ItemArray[2].ToString());
                    Thread.Sleep(2000);
                    StreetNum_Element.SendKeys(row.ItemArray[3].ToString());
                    //Selecting a street type from dropdown
                    var selectElement_street = new SelectElement(StreetType_Element);
                    selectElement_street.SelectByText(row.ItemArray[4].ToString());

                    Suburb_Element.SendKeys(row.ItemArray[5].ToString());
                    //Selecting a state from dropdown
                    var selectElement_state = new SelectElement(State_Element);
                    selectElement_state.SelectByText(row.ItemArray[6].ToString());

                    Postcode_Element.SendKeys(row.ItemArray[7].ToString());
                    State_Element.Click();
                    //Thread.Sleep(5000);
                    //Selecting LGA Code
                    waitAgency.Until(c => c.FindElement(SvcLGACode_element));
                    Driver.FindElement(SvcLGACode_element).Click();
                    waitAgency.Until(c => c.FindElement(LgaCode_Element));
                    Driver.FindElement(LgaCode_Element).Click();

                    waitAgency.Until(c => c.FindElement(LgaRadio_Element));
                    Driver.FindElement(LgaRadio_Element).Click();
                    var selectElement_lga = new SelectElement(Lga_Element);
                    selectElement_lga.SelectByText(row.ItemArray[8].ToString());
                    LgaSubmitButton_Element.Click();
                    Thread.Sleep(1000);
                    Driver.FindElement(Tele_Element).SendKeys(row.ItemArray[9].ToString());
                    //Thread.Sleep(2000);
                    Email_Element.SendKeys(row.ItemArray[10].ToString());
                    //Selecting emergency title from dropdown
                    var selectElement_title = new SelectElement(TitleEmergency_Element);
                    selectElement_title.SelectByText(row.ItemArray[11].ToString());
                    //Filling emergecy details
                    GivenNameEmergency_Element.SendKeys(row.ItemArray[12].ToString());
                    FamilyNameEmergency_Element.SendKeys(row.ItemArray[13].ToString());                   
                    EmailEmergency_Element.SendKeys(row.ItemArray[14].ToString());                   
                    PositionEmergency_Element.SendKeys(row.ItemArray[15].ToString());
                    TeleEmergency_Element.SendKeys(row.ItemArray[16].ToString());
                    MobileEmergency_Element.SendKeys(row.ItemArray[17].ToString());                   
                    //Selecting council facility radio button
                    SeviceOperatorCouncil_Element.Click();
                    //Forced wait for 2 seconds
                    Thread.Sleep(2000);
                    //Filling in details on next page
                    NextButton_Element.Click();
                    InsuranceCover_Element.Click();
                    // Select EYM = 'Yes'
                    EymYes_Element.Click();
                    EducationNature_Element.Click();
                    //Selecting quality rating
                    var selectElement_rating = new SelectElement(QualityRating_Element);
                    selectElement_rating.SelectByText(row.ItemArray[18].ToString());
                    ProgramOffered_Element.Click();
                    Thread.Sleep(2000);
                    SubmitButton_Element.Click();
                    Thread.Sleep(3000);
                    var winHandleBefore = Driver.CurrentWindowHandle;
                    UploadButton_Element.Click();
                    Thread.Sleep(2000);
                    //Switch to upload window
                    Driver.SwitchTo().Window(Driver.WindowHandles.Last());
                    //Uploading the file using keyboard actions
                    Thread.Sleep(2000);
                    ChooseFile_Element.Click();
                    Thread.Sleep(2000);
                    var fileNamePath = CommonFunctions.GetResourceValue("FileUpload_EymService");
                    SendKeys.SendWait(fileNamePath);
                    SendKeys.SendWait(@"{Enter}");

                    Thread.Sleep(2000);
                    UploadButtonFile_Element.Click();
                    SaveButtonFile_Element.Click();
                    //Brings the control back to original window
                    Driver.SwitchTo().Window(winHandleBefore);
                    
                }
             }
             catch (System.EntryPointNotFoundException ex)
             {
                 System.Console.WriteLine(ex.ToString());
                
            }
            return NewService;
        }

        internal void ConfirmDeleteService_Assert(string service)
        {
          
            string list = Driver.FindElements(By.XPath("//a[contains(text(),'" + service + "')]")).Count.ToString();
            //if service text is found the count will be 1, otherwise list will be 0
            if (list=="1")
            {
                throw new Exception("Service is not ceased");
            }
        }


        internal void DeleteService(string service)
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(EditButton_Element));  
            Driver.FindElement(EditButton_Element).Click();
            Thread.Sleep(2000);
            waitAgency.Until(c => c.FindElement(RemoveButton_Element));
            Driver.FindElement(RemoveButton_Element).Click();
            //Click on edit button to fill more details
            waitAgency.Until(c => c.FindElement(EditButton_Remove));
            Driver.FindElement(EditButton_Remove).Click();
        }
        internal void ConfirmDeleteService(string reason)
        {
            Thread.Sleep(2000);
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(RadioButton_Element));
            Driver.FindElement(RadioButton_Element).Click();
            waitAgency.Until(c => c.FindElement(CeaseDate_Element));
            Driver.FindElement(CeaseDate_Element).Click();
            SelectDate_Today.Click();
            waitAgency.Until(c => c.FindElement(ReasonTextBox_Element));
            Driver.FindElement(ReasonTextBox_Element).Click();
            Driver.FindElement(ReasonTextBox_Element).SendKeys(reason);
            //Click on submit button to submit the request and close the form 
            waitAgency.Until(c => c.FindElement(SubmitButton_Remove));
            Driver.FindElement(SubmitButton_Remove).Click();  
        }
        internal void ConfirmDeleteService_PastDate(string reason)
        {
            Thread.Sleep(2000);
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(RadioButton_Element));
            Driver.FindElement(RadioButton_Element).Click();
            waitAgency.Until(c => c.FindElement(CeaseDate_Element));
            Driver.FindElement(CeaseDate_Element).Click();
            //Enter a date from last year
            for (int i = 0; i < 12; i++)
            {
                waitAgency.Until(c => c.FindElement(Back_Button));
                Driver.FindElement(Back_Button).Click();
            }
            SelectDate_Past.Click();
            waitAgency.Until(c => c.FindElement(ReasonTextBox_Element));
            Driver.FindElement(ReasonTextBox_Element).Click();
            Driver.FindElement(ReasonTextBox_Element).SendKeys(reason);
            //Click on submit button to submit the request and close the form 
            waitAgency.Until(c => c.FindElement(SubmitButton_Remove));
            Driver.FindElement(SubmitButton_Remove).Click();
        }
        internal void SubmitForm()
        {
            LeagalCouncil_Element.Click();
            Thread.Sleep(2000);
            SubmitButton2_Element.Click();
            Thread.Sleep(2000);
        }

        internal void Verify_EymFunding_Approved()
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(EymFundingStatus_Approved));
            Assert.IsTrue(Driver.FindElement(EymFundingStatus_Approved).Displayed, "The service is not approved for EYM funding");
        }
        internal void RecommenceService(string service)
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(EditButton_Element));
            Driver.FindElement(EditButton_Element).Click();
            //Wait for dialog to open
            Thread.Sleep(2000);
            waitAgency.Until(c => c.FindElement(Recommence_Radio));
            Driver.FindElement(Recommence_Radio).Click();
            waitAgency.Until(c => c.FindElement(ApplyButton));
            Driver.FindElement(ApplyButton).Click();
        }
        internal void FillRecommenceForm(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));

                //Filling up details recommencing service
                                
                //fill date with today's date
                waitAgency.Until(c => c.FindElement(ServiceForm_element));
                Thread.Sleep(2000);
                Driver.FindElement(ServiceDateCommenced_element).Click();
                Driver.FindElement(ServiceDateCommenced_day_element).Click();

                //select yes for Does this service operate out of a Council owned facility?
                waitAgency.Until(c => c.FindElement(SvcOutofCouncilYes_radio_element));
                Driver.FindElement(SvcOutofCouncilYes_radio_element).Click();

                //click Next
                NextButton_Element.Click();

                //select yes for Is Altona West Kindergarten an incorporated association, co-operative or company limited by guarantee? (Required)
                waitAgency.Until(c => c.FindElement(SvcIsAssociation_radio_element));
                Driver.FindElement(SvcIsAssociation_radio_element).Click();

                //enter legal entity name 
                waitAgency.Until(c => c.FindElement(SvcLegalEntity_element));
                Driver.FindElement(SvcLegalEntity_element).SendKeys(row.ItemArray[0].ToString());

                //select yes for Do you wish to include early years management along with this application?
                //waitAgency.Until(c => c.FindElement(SvcEYM_Yes_radio_element));
                //Driver.FindElement(SvcEYM_Yes_radio_element).Click();

                //select Preschool/Kindergarten for What is the core nature of education and care provided at this location?
                waitAgency.Until(c => c.FindElement(SvcNatureofEducCare_radio_element));
                Driver.FindElement(SvcNatureofEducCare_radio_element).Click();

                //select value for What is this service's National Quality Framework rating?
                IWebElement listNQFr = Driver.FindElement(SvcNationalQualityFrameworkRating_dropdown_element);
                SelectElement selectNQFr = new SelectElement(listNQFr);                
                selectNQFr.SelectByText(row.ItemArray[1].ToString());

                //select Yes for Is the funded kindergarten the only program offered at this service ?
                waitAgency.Until(c => c.FindElement(SvcFundedKindergarten_radio_element));
                Driver.FindElement(SvcFundedKindergarten_radio_element).Click();

                //select Submit
                SubmitButton_Element.Click();

                //select Exit button in EYM Years Mgt screen
                //waitAgency.Until(c => c.FindElement(EymForm_ExitButton));
                //Driver.FindElement(EymForm_ExitButton).Click(); 

                //Navigate to service list page
                //Driver.Navigate().Back();
            }
        }
        internal void FillRecommenceFormFromEYMtoNotEYM(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(100));

                //Filling up details recommencing service

                //fill date with today's date
                waitAgency.Until(c => c.FindElement(ServiceForm_element));
                Thread.Sleep(2000);
                Driver.FindElement(ServiceDateCommenced_element).Click();
                Driver.FindElement(ServiceDateCommenced_day_element).Click();

                //Fill Number Address
                waitAgency.Until(c => c.FindElement(SvcNumber_element));
                Driver.FindElement(SvcNumber_element).Clear();
                Driver.FindElement(SvcNumber_element).SendKeys(row.ItemArray[0].ToString());

                //Fill Street Address
                waitAgency.Until(c => c.FindElement(SvcStreet_element));
                Driver.FindElement(SvcStreet_element).Clear();
                Driver.FindElement(SvcStreet_element).SendKeys(row.ItemArray[1].ToString());

                //Select Street Type Address
                waitAgency.Until(c => c.FindElement(SvcStreetType_element));

                IWebElement listStreetType = Driver.FindElement(SvcStreetType_element);
                SelectElement selectStreetType = new SelectElement(listStreetType);
                selectStreetType.SelectByText(row.ItemArray[2].ToString());

                //Select Suburb Address
                waitAgency.Until(c => c.FindElement(SvcSuburb_element));
                Driver.FindElement(SvcSuburb_element).Clear();
                Driver.FindElement(SvcSuburb_element).SendKeys(row.ItemArray[3].ToString());

                //Select State Address
                waitAgency.Until(c => c.FindElement(SvcState_element));

                IWebElement listState = Driver.FindElement(SvcState_element);
                SelectElement selectState = new SelectElement(listState);
                selectState.SelectByText(row.ItemArray[4].ToString());

                //Fill Postcode Address
                waitAgency.Until(c => c.FindElement(SvcPostCode_element));
                Driver.FindElement(SvcPostCode_element).Clear();
                Driver.FindElement(SvcPostCode_element).SendKeys(row.ItemArray[5].ToString());

                //Selecting LGA Code
                waitAgency.Until(c => c.FindElement(SvcLGACode_element));
                Driver.FindElement(SvcLGACode_element).Click();
                waitAgency.Until(c => c.FindElement(LgaCode_Element));
                Driver.FindElement(LgaCode_Element).Click();

                Thread.Sleep(3000);
                waitAgency.Until(c => c.FindElement(LgaRadio_Element));
                Driver.FindElement(LgaRadio_Element).Click();
                var selectElement_lga = new SelectElement(Lga_Element);
                selectElement_lga.SelectByText(row.ItemArray[6].ToString());
                LgaSubmitButton_Element.Click();

                //Fill Telephone Address
                waitAgency.Until(c => c.FindElement(SvcTelephone_element));
                Driver.FindElement(SvcTelephone_element).Clear();
                Driver.FindElement(SvcTelephone_element).SendKeys(row.ItemArray[7].ToString());

                //Fill Email Address
                waitAgency.Until(c => c.FindElement(SvcEmail_element));
                Driver.FindElement(SvcEmail_element).Clear();
                Driver.FindElement(SvcEmail_element).SendKeys(row.ItemArray[8].ToString());

                //Select ECD Title
                waitAgency.Until(c => c.FindElement(SvcECDTitle_element));

                IWebElement listECDTitle = Driver.FindElement(SvcECDTitle_element);
                SelectElement selectECDTitle = new SelectElement(listECDTitle);
                selectECDTitle.SelectByText(row.ItemArray[9].ToString());

                //Fill ECD First Name
                waitAgency.Until(c => c.FindElement(SvcECDGivenName_element));
                Driver.FindElement(SvcECDGivenName_element).Clear();
                Driver.FindElement(SvcECDGivenName_element).SendKeys(row.ItemArray[10].ToString());

                //Fill ECD Last Name
                waitAgency.Until(c => c.FindElement(SvcECDFamilyName_element));
                Driver.FindElement(SvcECDFamilyName_element).Clear();
                Driver.FindElement(SvcECDFamilyName_element).SendKeys(row.ItemArray[11].ToString());

                //Fill ECD Email
                waitAgency.Until(c => c.FindElement(SvcECDEmail_element));
                Driver.FindElement(SvcECDEmail_element).Clear();
                Driver.FindElement(SvcECDEmail_element).SendKeys(row.ItemArray[12].ToString());

                //Fill ECD Position
                waitAgency.Until(c => c.FindElement(SvcECDPosition_element));
                Driver.FindElement(SvcECDPosition_element).Clear();
                Driver.FindElement(SvcECDPosition_element).SendKeys(row.ItemArray[13].ToString());

                //Fill ECD Telephone
                waitAgency.Until(c => c.FindElement(SvcECDTelephone_element));
                Driver.FindElement(SvcECDTelephone_element).Clear();
                Driver.FindElement(SvcECDTelephone_element).SendKeys(row.ItemArray[14].ToString());

                //Fill ECD Mobile
                waitAgency.Until(c => c.FindElement(SvcECDMobile_element));
                Driver.FindElement(SvcECDMobile_element).Clear();
                Driver.FindElement(SvcECDMobile_element).SendKeys(row.ItemArray[15].ToString());

                //select yes for Does this service operate out of a Council owned facility?
                waitAgency.Until(c => c.FindElement(SvcOutofCouncilYes_radio_element));
                Driver.FindElement(SvcOutofCouncilYes_radio_element).Click();

                //click Next
                NextButton_Element.Click();

                //select yes for Is Altona West Kindergarten an incorporated association, co-operative or company limited by guarantee? (Required)
                waitAgency.Until(c => c.FindElement(SvcIsAssociation_radio_element));
                Driver.FindElement(SvcIsAssociation_radio_element).Click();

                //enter legal entity name 
                waitAgency.Until(c => c.FindElement(SvcLegalEntity_element));
                Driver.FindElement(SvcLegalEntity_element).SendKeys(row.ItemArray[16].ToString());

                //select No for Do you wish to include early years management along with this application?
                waitAgency.Until(c => c.FindElement(SvcEYM_No_radio_element));
                Driver.FindElement(SvcEYM_No_radio_element).Click();

                //select Preschool/Kindergarten for What is the core nature of education and care provided at this location?
                waitAgency.Until(c => c.FindElement(SvcNatureofEducCare_radio_element));
                Driver.FindElement(SvcNatureofEducCare_radio_element).Click();

                //select value for What is this service's National Quality Framework rating?
                IWebElement listNQFr = Driver.FindElement(SvcNationalQualityFrameworkRating_dropdown_element);
                SelectElement selectNQFr = new SelectElement(listNQFr);
                selectNQFr.SelectByText(row.ItemArray[17].ToString());

                //select Yes for Is the funded kindergarten the only program offered at this service ?
                waitAgency.Until(c => c.FindElement(SvcFundedKindergarten_radio_element));
                Driver.FindElement(SvcFundedKindergarten_radio_element).Click();

                //select Submit
                Thread.Sleep(5000);
                SubmitButton_Element.Click();
                Thread.Sleep(5000);
            }
        }
        internal void FillRecommenceFormFromEYMtoEYM(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));

                //Filling up details recommencing service

                //fill date with today's date
                waitAgency.Until(c => c.FindElement(ServiceForm_element));                
                Thread.Sleep(2000);
                Driver.FindElement(ServiceDateCommenced_element).Click();
                Driver.FindElement(ServiceDateCommenced_day_element).Click();

                //Fill Number Address
                waitAgency.Until(c => c.FindElement(SvcNumber_element));
                Driver.FindElement(SvcNumber_element).Clear();
                Driver.FindElement(SvcNumber_element).SendKeys(row.ItemArray[0].ToString());

                //Fill Street Address
                waitAgency.Until(c => c.FindElement(SvcStreet_element));
                Driver.FindElement(SvcStreet_element).Clear();
                Driver.FindElement(SvcStreet_element).SendKeys(row.ItemArray[1].ToString());

                //Select Street Type Address
                waitAgency.Until(c => c.FindElement(SvcStreetType_element));
                
                IWebElement listStreetType = Driver.FindElement(SvcStreetType_element);
                SelectElement selectStreetType = new SelectElement(listStreetType);
                selectStreetType.SelectByText(row.ItemArray[2].ToString());

                //Select Suburb Address
                waitAgency.Until(c => c.FindElement(SvcSuburb_element));
                Driver.FindElement(SvcSuburb_element).Clear();
                Driver.FindElement(SvcSuburb_element).SendKeys(row.ItemArray[3].ToString());                

                //Select State Address
                waitAgency.Until(c => c.FindElement(SvcState_element));

                IWebElement listState = Driver.FindElement(SvcState_element);
                SelectElement selectState= new SelectElement(listState);
                selectState.SelectByText(row.ItemArray[4].ToString());

                //Fill Postcode Address
                waitAgency.Until(c => c.FindElement(SvcPostCode_element));
                Driver.FindElement(SvcPostCode_element).Clear();
                Driver.FindElement(SvcPostCode_element).SendKeys(row.ItemArray[5].ToString());
                                
                //Selecting LGA Code
                waitAgency.Until(c => c.FindElement(SvcLGACode_element));
                Driver.FindElement(SvcLGACode_element).Click();
                waitAgency.Until(c => c.FindElement(LgaCode_Element));
                Driver.FindElement(LgaCode_Element).Click();

                Thread.Sleep(3000);
                waitAgency.Until(c => c.FindElement(LgaRadio_Element));
                Driver.FindElement(LgaRadio_Element).Click();
                var selectElement_lga = new SelectElement(Lga_Element);
                selectElement_lga.SelectByText(row.ItemArray[6].ToString());
                LgaSubmitButton_Element.Click();

                //Fill Telephone Address
                waitAgency.Until(c => c.FindElement(SvcTelephone_element));
                Driver.FindElement(SvcTelephone_element).Clear();
                Driver.FindElement(SvcTelephone_element).SendKeys(row.ItemArray[7].ToString());

                //Fill Email Address
                waitAgency.Until(c => c.FindElement(SvcEmail_element));
                Driver.FindElement(SvcEmail_element).Clear();
                Driver.FindElement(SvcEmail_element).SendKeys(row.ItemArray[8].ToString());

                //Select ECD Title
                waitAgency.Until(c => c.FindElement(SvcECDTitle_element));

                IWebElement listECDTitle = Driver.FindElement(SvcECDTitle_element);
                SelectElement selectECDTitle = new SelectElement(listECDTitle);
                selectECDTitle.SelectByText(row.ItemArray[9].ToString());

                //Fill ECD First Name
                waitAgency.Until(c => c.FindElement(SvcECDGivenName_element));
                Driver.FindElement(SvcECDGivenName_element).Clear();
                Driver.FindElement(SvcECDGivenName_element).SendKeys(row.ItemArray[10].ToString());

                //Fill ECD Last Name
                waitAgency.Until(c => c.FindElement(SvcECDFamilyName_element));
                Driver.FindElement(SvcECDFamilyName_element).Clear();
                Driver.FindElement(SvcECDFamilyName_element).SendKeys(row.ItemArray[11].ToString());
                
                //Fill ECD Email
                waitAgency.Until(c => c.FindElement(SvcECDEmail_element));
                Driver.FindElement(SvcECDEmail_element).Clear();
                Driver.FindElement(SvcECDEmail_element).SendKeys(row.ItemArray[12].ToString());

                //Fill ECD Position
                waitAgency.Until(c => c.FindElement(SvcECDPosition_element));
                Driver.FindElement(SvcECDPosition_element).Clear();
                Driver.FindElement(SvcECDPosition_element).SendKeys(row.ItemArray[13].ToString());

                //Fill ECD Telephone
                waitAgency.Until(c => c.FindElement(SvcECDTelephone_element));
                Driver.FindElement(SvcECDTelephone_element).Clear();
                Driver.FindElement(SvcECDTelephone_element).SendKeys(row.ItemArray[14].ToString());

                //Fill ECD Mobile
                waitAgency.Until(c => c.FindElement(SvcECDMobile_element));
                Driver.FindElement(SvcECDMobile_element).Clear();
                Driver.FindElement(SvcECDMobile_element).SendKeys(row.ItemArray[15].ToString());

                //select yes for Does this service operate out of a Council owned facility?
                waitAgency.Until(c => c.FindElement(SvcOutofCouncilYes_radio_element));
                Driver.FindElement(SvcOutofCouncilYes_radio_element).Click();

                //click Next
                NextButton_Element.Click();

                //select yes for Is Altona West Kindergarten an incorporated association, co-operative or company limited by guarantee? (Required)
                waitAgency.Until(c => c.FindElement(SvcIsAssociation_radio_element));
                Driver.FindElement(SvcIsAssociation_radio_element).Click();

                //enter legal entity name 
                waitAgency.Until(c => c.FindElement(SvcLegalEntity_element));
                Driver.FindElement(SvcLegalEntity_element).SendKeys(row.ItemArray[16].ToString());

                //select yes for Do you wish to include early years management along with this application?
                waitAgency.Until(c => c.FindElement(SvcEYM_Yes_radio_element));
                Driver.FindElement(SvcEYM_Yes_radio_element).Click();

                //select Preschool/Kindergarten for What is the core nature of education and care provided at this location?
                waitAgency.Until(c => c.FindElement(SvcNatureofEducCare_radio_element));
                Driver.FindElement(SvcNatureofEducCare_radio_element).Click();

                //select value for What is this service's National Quality Framework rating?
                IWebElement listNQFr = Driver.FindElement(SvcNationalQualityFrameworkRating_dropdown_element);
                SelectElement selectNQFr = new SelectElement(listNQFr);
                selectNQFr.SelectByText(row.ItemArray[17].ToString());

                //select Yes for Is the funded kindergarten the only program offered at this service ?
                waitAgency.Until(c => c.FindElement(SvcFundedKindergarten_radio_element));
                Driver.FindElement(SvcFundedKindergarten_radio_element).Click();

                //select Submit
                Thread.Sleep(5000);
                SubmitButton_Element.Click();
                Thread.Sleep(5000);

                Thread.Sleep(2000);
                //select Exit button in EYM Years Mgt screen
                waitAgency.Until(c => c.FindElement(EymForm_element));
                waitAgency.Until(c => c.FindElement(EymForm_ExitButton));
                Driver.FindElement(EymForm_ExitButton).Click();

                Thread.Sleep(2000);
                IAlert ServiceAlert = Driver.SwitchTo().Alert();                
                ServiceAlert.Accept();
                Thread.Sleep(2000);

                //Navigate to service list page
                //Driver.Navigate().Back();
            }
        }

        internal void FillRecommenceFormFromEYMtoEYMSUBMIITED(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));

                //Filling up details recommencing service

                //fill date with today's date
                waitAgency.Until(c => c.FindElement(ServiceForm_element));
                Thread.Sleep(2000);
                Driver.FindElement(ServiceDateCommenced_element).Click();
                Driver.FindElement(ServiceDateCommenced_day_element).Click();

                //Fill Number Address
                waitAgency.Until(c => c.FindElement(SvcNumber_element));
                Driver.FindElement(SvcNumber_element).Clear();
                Driver.FindElement(SvcNumber_element).SendKeys(row.ItemArray[0].ToString());

                //Fill Street Address
                waitAgency.Until(c => c.FindElement(SvcStreet_element));
                Driver.FindElement(SvcStreet_element).Clear();
                Driver.FindElement(SvcStreet_element).SendKeys(row.ItemArray[1].ToString());

                //Select Street Type Address
                waitAgency.Until(c => c.FindElement(SvcStreetType_element));

                IWebElement listStreetType = Driver.FindElement(SvcStreetType_element);
                SelectElement selectStreetType = new SelectElement(listStreetType);
                selectStreetType.SelectByText(row.ItemArray[2].ToString());

                //Select Suburb Address
                waitAgency.Until(c => c.FindElement(SvcSuburb_element));
                Driver.FindElement(SvcSuburb_element).Clear();
                Driver.FindElement(SvcSuburb_element).SendKeys(row.ItemArray[3].ToString());

                //Select State Address
                waitAgency.Until(c => c.FindElement(SvcState_element));

                IWebElement listState = Driver.FindElement(SvcState_element);
                SelectElement selectState = new SelectElement(listState);
                selectState.SelectByText(row.ItemArray[4].ToString());

                //Fill Postcode Address
                waitAgency.Until(c => c.FindElement(SvcPostCode_element));
                Driver.FindElement(SvcPostCode_element).Clear();
                Driver.FindElement(SvcPostCode_element).SendKeys(row.ItemArray[5].ToString());

                //Selecting LGA Code
                waitAgency.Until(c => c.FindElement(SvcLGACode_element));
                Driver.FindElement(SvcLGACode_element).Click();
                waitAgency.Until(c => c.FindElement(LgaCode_Element));
                Driver.FindElement(LgaCode_Element).Click();

                Thread.Sleep(3000);
                waitAgency.Until(c => c.FindElement(LgaRadio_Element));
                Driver.FindElement(LgaRadio_Element).Click();
                var selectElement_lga = new SelectElement(Lga_Element);
                selectElement_lga.SelectByText(row.ItemArray[6].ToString());
                LgaSubmitButton_Element.Click();

                //Fill Telephone Address
                waitAgency.Until(c => c.FindElement(SvcTelephone_element));
                Driver.FindElement(SvcTelephone_element).Clear();
                Driver.FindElement(SvcTelephone_element).SendKeys(row.ItemArray[7].ToString());

                //Fill Email Address
                waitAgency.Until(c => c.FindElement(SvcEmail_element));
                Driver.FindElement(SvcEmail_element).Clear();
                Driver.FindElement(SvcEmail_element).SendKeys(row.ItemArray[8].ToString());

                //Select ECD Title
                waitAgency.Until(c => c.FindElement(SvcECDTitle_element));

                IWebElement listECDTitle = Driver.FindElement(SvcECDTitle_element);
                SelectElement selectECDTitle = new SelectElement(listECDTitle);
                selectECDTitle.SelectByText(row.ItemArray[9].ToString());

                //Fill ECD First Name
                waitAgency.Until(c => c.FindElement(SvcECDGivenName_element));
                Driver.FindElement(SvcECDGivenName_element).Clear();
                Driver.FindElement(SvcECDGivenName_element).SendKeys(row.ItemArray[10].ToString());

                //Fill ECD Last Name
                waitAgency.Until(c => c.FindElement(SvcECDFamilyName_element));
                Driver.FindElement(SvcECDFamilyName_element).Clear();
                Driver.FindElement(SvcECDFamilyName_element).SendKeys(row.ItemArray[11].ToString());

                //Fill ECD Email
                waitAgency.Until(c => c.FindElement(SvcECDEmail_element));
                Driver.FindElement(SvcECDEmail_element).Clear();
                Driver.FindElement(SvcECDEmail_element).SendKeys(row.ItemArray[12].ToString());

                //Fill ECD Position
                waitAgency.Until(c => c.FindElement(SvcECDPosition_element));
                Driver.FindElement(SvcECDPosition_element).Clear();
                Driver.FindElement(SvcECDPosition_element).SendKeys(row.ItemArray[13].ToString());

                //Fill ECD Telephone
                waitAgency.Until(c => c.FindElement(SvcECDTelephone_element));
                Driver.FindElement(SvcECDTelephone_element).Clear();
                Driver.FindElement(SvcECDTelephone_element).SendKeys(row.ItemArray[14].ToString());

                //Fill ECD Mobile
                waitAgency.Until(c => c.FindElement(SvcECDMobile_element));
                Driver.FindElement(SvcECDMobile_element).Clear();
                Driver.FindElement(SvcECDMobile_element).SendKeys(row.ItemArray[15].ToString());

                //select yes for Does this service operate out of a Council owned facility?
                waitAgency.Until(c => c.FindElement(SvcOutofCouncilYes_radio_element));
                Driver.FindElement(SvcOutofCouncilYes_radio_element).Click();

                //click Next
                NextButton_Element.Click();

                //select yes for Is Altona West Kindergarten an incorporated association, co-operative or company limited by guarantee? (Required)
                waitAgency.Until(c => c.FindElement(SvcIsAssociation_radio_element));
                Driver.FindElement(SvcIsAssociation_radio_element).Click();

                //enter legal entity name 
                waitAgency.Until(c => c.FindElement(SvcLegalEntity_element));
                Driver.FindElement(SvcLegalEntity_element).SendKeys(row.ItemArray[16].ToString());

                //select yes for Do you wish to include early years management along with this application?
                waitAgency.Until(c => c.FindElement(SvcEYM_Yes_radio_element));
                Driver.FindElement(SvcEYM_Yes_radio_element).Click();

                //select Preschool/Kindergarten for What is the core nature of education and care provided at this location?
                waitAgency.Until(c => c.FindElement(SvcNatureofEducCare_radio_element));
                Driver.FindElement(SvcNatureofEducCare_radio_element).Click();

                //select value for What is this service's National Quality Framework rating?
                IWebElement listNQFr = Driver.FindElement(SvcNationalQualityFrameworkRating_dropdown_element);
                SelectElement selectNQFr = new SelectElement(listNQFr);
                selectNQFr.SelectByText(row.ItemArray[17].ToString());

                //select Yes for Is the funded kindergarten the only program offered at this service ?
                waitAgency.Until(c => c.FindElement(SvcFundedKindergarten_radio_element));
                Driver.FindElement(SvcFundedKindergarten_radio_element).Click();

                //select Submit
                Thread.Sleep(5000);
                SubmitButton_Element.Click();
                Thread.Sleep(5000);

                Thread.Sleep(2000);
                //select Exit button in EYM Years Mgt screen
                waitAgency.Until(c => c.FindElement(EymForm_element));

                var winHandleBefore = Driver.CurrentWindowHandle;
                UploadButton_Element.Click();
                Thread.Sleep(2000);
                //Switch to upload window
                Driver.SwitchTo().Window(Driver.WindowHandles.Last());
                //Uploading the file using keyboard actions
                Thread.Sleep(2000);
                ChooseFile_Element.Click();
                Thread.Sleep(2000);
                var fileNamePath = CommonFunctions.GetResourceValue("FileUpload_EymService");
                SendKeys.SendWait(fileNamePath);
                SendKeys.SendWait(@"{Enter}");

                Thread.Sleep(2000);
                UploadButtonFile_Element.Click();                
                SaveButtonFile_Element.Click();
                //Brings the control back to original window
                Driver.SwitchTo().Window(winHandleBefore);

                waitAgency.Until(c => c.FindElement(EymForm_element));
                waitAgency.Until(c => c.FindElement(EymFormEymOrgApproval_No_element));
                Driver.FindElement(EymFormEymOrgApproval_No_element).Click();
                waitAgency.Until(c => c.FindElement(EymForm_SubmitButton));
                Driver.FindElement(EymForm_SubmitButton).Click();

                Thread.Sleep(2000);
                //Navigate to service list page
                //Driver.Navigate().Back();
            }
        }
        internal void Verify_Service_Recommenced(string service)
        {
            waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            waitAgency.Until(c => c.FindElement(EditButton_Element));
            Driver.FindElement(EditButton_Element).Click();
            //Wait for dialog to open
            Thread.Sleep(2000);
            waitAgency.Until(c => c.FindElement(Recommence_Radio));
            Assert.IsFalse(Driver.FindElement(Recommence_Radio).Enabled, "Recommence button is enabled.");
        }        
    }
 }
  